import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Spider here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public class Spider extends Actor
{
    private static int counter = 0;
    //private int counter = 0;
    private int move_n = 400;
    
    public void act() 
    {
        
        move(1);
        // counter++;
        // counter += 1;
        counter = counter + 1;
        if(counter > move_n){
            Greenfoot.stop();
        }
        
        if(doTurn()){
            turn(90);
        }
    }    
    
    public boolean doTurn(){
        boolean isTurn = false;
        
        if(Greenfoot.getRandomNumber(200) == 7){
            isTurn = true;
        }
        
        return isTurn;
    }
}
