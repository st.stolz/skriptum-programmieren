import greenfoot.*;  

public class Butterfly extends Actor
{
    int distance = 10;
    
    public void act() 
    {
        if(isAtEdge()){
            move(distance * -1);
            turn(180);
        }
        move(distance);
    }    
}
