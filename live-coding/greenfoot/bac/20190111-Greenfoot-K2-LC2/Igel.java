import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Ein Igel, der in der Welt bis zum Rand,
 * zurück zum anderen Rand und wieder von vorne
 * läuft
 */
public class Igel extends Actor
{
    /* Wie weit bewegt ein
     * move() den Igel?
     */ 
    int distance = 10;
    
    /* Der Konstruktor wird bei Erzeugung des
     * Objekt aufgerufen. Hier ist er leer und
     * tut daher nichts.
     */
    public Igel(){ 
    
    }
    
    public void act() 
    {        
        /* Wenn der Igel den Rand erreicht,
         * wird der distance Wert umgekehrt. 
         */        
        if(isAtEdge()){
            distance = distance * -1;
        }
        /* Bewege den Igel um den Wert, der in
         * distance gespeichert ist. 
         */
        move(distance);
    }    
}
