import greenfoot.*;  

public class Butterfly extends Actor
{
    int distance = 10;
    int turnAngle = 10;
    
    public void act() 
    {
        turnAngle = Greenfoot.getRandomNumber(90);
        if(isAtEdge()){
            move(distance * -1);
            turn(180);
        }
        move(distance);
        turn(turnAngle);
    }    
}
