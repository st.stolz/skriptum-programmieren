import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Flower here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Flower extends Actor
{
    /**
     * Act - do whatever the Flower wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    int counter = 0;
    
    public void act() 
    {
        checkTouching();
    }    
    
    public void checkTouching(){       
        
            if(isTouching(Butterfly.class)){
                counter = counter + 1;
            }
            if(counter >= 20){
                counter = 0;
                removeMe();        
            }
            
     }
     
public void removeMe(){
     World world = getWorld();
     world.removeObject(this);
}
}
