import greenfoot.*;  

public class Butterfly extends Actor
{
    int distance = 10;
    
    public void act() 
    {
        if(isAtEdge()){            
            turn(Greenfoot.getRandomNumber(360));            
        }  
        if(Greenfoot.getRandomNumber(20)==1){
            turn(20);
        }
        move(distance);
        
    }  
    
    
}