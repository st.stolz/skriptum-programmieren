import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Ein Igel, der bis zum Rand geht,
 * sich umdreht, wieder zurück geht 
 * und von vorne.
 */
public class Igel extends Actor
{
    
    /* Konstruktor ohne Implementierung 
     */
    public Igel(){ 
    
    }
    
    public void act() 
    {   
        /* Die Distanz wird in jedem act()
         * Aufruf neu gesetzt. Das tut aber
         * nichts, weil sich der Actor immer
         * um 10 in Blickrichtung bewegen soll. 
         */
        int distance = 10;
        /* Wenn der Igel am Rand ist, dreht
         * er sich. 
         */
        if(isAtEdge()){
            turn(180);
        }
        /* Bewegt den igel um den Wert in 
         * distance. 
         */
        move(distance);
    }    
}
