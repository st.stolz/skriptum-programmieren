import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class GameField extends World
{
    private final int NUM_ANTS = 10;
    private final String[] ANT_IMAGES = {"ant.png","ant3.png"};
    
    private Ant[] ants;  
    private GreenfootImage[] antImages;
    
    public GameField()
    {    
        super(600, 400, 1);         
        addInitialObjects();
    }
    
    
    private void addInitialObjects(){
        
        Apple apple = new Apple();
        addObject(apple, 440, 200);
        
        antImages = new GreenfootImage[ANT_IMAGES.length];
        
        antImages[0] = new GreenfootImage("ant.png");
        antImages[1] = new GreenfootImage("ant3.png");
        
        ants = new Ant[NUM_ANTS];
        
        int counter = 0;
        while(counter < NUM_ANTS){
            
            ants[counter] = new Ant();
            
            ants[counter].setImage(antImages[Greenfoot.getRandomNumber(ANT_IMAGES.length)]);
            
            addObject(ants[counter], Greenfoot.getRandomNumber(getWidth()), 
                Greenfoot.getRandomNumber(getHeight()));
                
            counter ++;
        }
    }
}
