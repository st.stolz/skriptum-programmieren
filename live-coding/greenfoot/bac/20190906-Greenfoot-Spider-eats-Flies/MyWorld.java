import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.LinkedList;

public class MyWorld extends World
{

    private LinkedList<Fly> flies;
    
    public MyWorld()
    {            
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1);        
        setup();
    }
    
    public void setup(){
        
        flies = new LinkedList<>();
        
        int counter = 0;
        while(counter < 20){
            Fly fly = new Fly();
            flies.add(fly); 
            addObject(fly, Greenfoot.getRandomNumber(600), Greenfoot.getRandomNumber(400));
            counter++;
        }
        
        addObject(new Spider(), 300, 200);
    }
}
