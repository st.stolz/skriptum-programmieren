import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class Spider extends Actor
{
    private int fliesToCatch;
    
    public Spider(){
        fliesToCatch = 10;
    }
    
    public void act()
    {
        if(isTouching(Fly.class)){
            removeTouching(Fly.class);
            fliesToCatch = fliesToCatch -1;
        }
        if(fliesToCatch < 1){
            Greenfoot.stop();
        }
    }    
}
