import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Players here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Players extends Actor
{
    int movingSpeed = 0;
    
    public void act() 
    {
        
    } 
    
    public void randomMove(){
        
        // World holen
        // Methode java.util.List getObjects(java.lang.Class<A> cls)
        // verwenden, um den Apfel zu bekommen. Auf diese Liste get(0) anwenden, 
        // um den Apfel zu bekommen
        // Vom Apfel mit getX() und getY() die Position holen
        // und dann für die Drehung mit void turnTowards(int x, int y) verwenden 
        // (zu einer gewissen Wahrscheinlickeit)

        
        // selten eine große Richtungsänderung
        if(Greenfoot.getRandomNumber(100) == 9)
            turn(Greenfoot.getRandomNumber(360));
            
        // Häufig eine kleine Richtungsänderung
        if(Greenfoot.getRandomNumber(10) == 9)
            turn(Greenfoot.getRandomNumber(10));
            
        // Drehung am Rand bis wieder davon weg
        if(isAtEdge()) 
            turn(Greenfoot.getRandomNumber(360));
        
        // Häufig Speed wenig ändern
        if(Greenfoot.getRandomNumber(10) == 9)
            movingSpeed = movingSpeed + Greenfoot.getRandomNumber(3)-1;
            
        // Selten Speed heftig ändern
        if(Greenfoot.getRandomNumber(50) == 9)
            movingSpeed = movingSpeed + Greenfoot.getRandomNumber(11)-5;
            
        // Speed nicht unter 0
        if(movingSpeed < 0)
            movingSpeed = 0;
            
        // Speed nicht über ...
        if(movingSpeed > 10)
            movingSpeed = 10;
            
        move(movingSpeed);
    }
    
    
}
