import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Erzeugung der Welt mit allen Objekten
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameField extends World
{
    // finale Variablen (können nicht verändert werden)
    // Anzahl der Ameisen
    private final int NUM_ANTS = 500;
    // Anzahl der Bilder für verschiedene Ameisen
    private final int NUM_ANT_IMAGES = 3;
    
    // Deklaration Array für Ameisen
    private Ant[] ants;  
    // Deklaration Array für Ameisenbilder
    private GreenfootImage[] antImages;
    
    public GameField()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1);
        addInitialObjects();
    }
    
    
    /**
     * Mit addInitialObjects werden die Objekte in die Welt
     * 
     *
     */
    private void addInitialObjects(){
        
        // In finaler Notation ein Apfel Objekt hinzufügen
        // dient derzeit nur als Hintergrund
        Apple apple = new Apple();
        addObject(apple, 440, 200);
        
        // Definition (Erzeugen, reservieren Speicher) 
        // des Ameisenbilder Array
        antImages = new GreenfootImage[NUM_ANT_IMAGES];
        // Belegen der Speicherstellen mit den Bildern
        antImages[0] = new GreenfootImage("ant3.png");
        antImages[1] = new GreenfootImage("ant.png");
        antImages[2] = new GreenfootImage("ant-with-food.png");
        
        int counter = 0;
        // Definition (Erzeugen, reservieren Speicher) 
        // des Ameisen Array in der Größe NUM_ANTS
        ants = new Ant[NUM_ANTS];
        
        // die counter Variable für die Anzahl NUM_ANTS
        // hochzählen. So kann sie verwendet werden, um
        // auf jeden Index des initialisierten Array ants
        // zuzugreifen
        while(counter < NUM_ANTS){
            // Neues Ant Objekt
            ants[counter] = new Ant();
            // Eines der NUM_ANT_IMAGES Bilder zufällig wählen
            int antImageIndex = Greenfoot.getRandomNumber(NUM_ANT_IMAGES);
            ants[counter].setImage(antImages[antImageIndex]);
            ants[counter].antType = antImageIndex;
            // Objekt der Welt hinzufügen
            addObject(ants[counter], Greenfoot.getRandomNumber(getWidth()), 
                Greenfoot.getRandomNumber(getHeight()));
                
            counter = counter + 1;
        }
    }
}
