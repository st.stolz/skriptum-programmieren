import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class Players here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Players extends Actor
{
    int movingSpeed = 0;
    
    public void act() 
    {
        
    } 
    
    public void randomMove(){    
        int distance = 1000;
        int apple_x=400;
        int apple_y=250;
        
        List<Apple> apples = getWorld().getObjects(Apple.class);
        if(apples.size() > 0){
            Apple apple = apples.get(0);       
            apple_x = apple.getX();
            apple_y = apple.getY();                 
            distance = (int) Math.abs(Math.sqrt(Math.pow(apple_x - this.getX(),2)+Math.pow(apple_y - this.getY(),2)));
        }
        
        // Häufig eine kleine Richtungsänderung
         if(Greenfoot.getRandomNumber(5) == 1)
            turn(Greenfoot.getRandomNumber(10));
        else if(Greenfoot.getRandomNumber(20) == 9 && distance > 100){                        
            turnTowards(apple_x, apple_y);
        }
        
        // selten eine große Richtungsänderung
        else if(Greenfoot.getRandomNumber(100) == 9)
            turn(Greenfoot.getRandomNumber(360));
            
        
            
         
            
        // Drehung am Rand bis wieder davon weg
        if(isAtEdge()) 
            turn(Greenfoot.getRandomNumber(360));
        
        // Häufig Speed wenig ändern
        if(Greenfoot.getRandomNumber(10) == 9)
            movingSpeed = movingSpeed + Greenfoot.getRandomNumber(3)-1;
            
        // Selten Speed heftig ändern
        if(Greenfoot.getRandomNumber(50) == 9)
            movingSpeed = movingSpeed + Greenfoot.getRandomNumber(11)-5;
            
        // Speed nicht unter 0
        if(movingSpeed < 0)
            movingSpeed = 0;
            
        // Speed nicht über ...
        if(movingSpeed > 10)
            movingSpeed = 10;
            
        move(movingSpeed);
    }
    
    
}
