import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{

    Elefant elefant;
    Nilpferd nilpferd;
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        elefant = new Elefant();
        nilpferd = new Nilpferd();
        addObject(elefant, 100, 200);
        addObject(nilpferd, 500, 200);
    }
    public void act(){
        showText("Nilpferd: "+nilpferd.energy, 500, 100);
        showText("Elefant: "+elefant.energy, 100, 100);
    }
    
}
