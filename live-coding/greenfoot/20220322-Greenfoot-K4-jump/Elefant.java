import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class Elefant extends Actor
{
    public int energy = 100;
    public void act() 
    {
        if(! isTouching(Nilpferd.class)){
            move(1);
        }
        else{
            energy = energy - Greenfoot.getRandomNumber(10);
            if(energy < 0){
                getWorld().removeObject(this);
            }
        }
        
    }    
}
