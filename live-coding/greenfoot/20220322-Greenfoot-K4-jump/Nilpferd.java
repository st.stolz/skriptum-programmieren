import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Nilpferd here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Nilpferd extends Actor
{
    public int energy = 100;
    public void act() 
    {
        if(isTouching(Elefant.class)){            
            energy = energy - Greenfoot.getRandomNumber(10);
            if(energy < 0){
                getWorld().removeObject(this);
            }
        }
    }    
}
