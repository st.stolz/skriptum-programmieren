import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{

    int counter = 0;
    Frog frog = new Frog();
    
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1);         
        addObject(frog,200,200);
    }
    
    public void act(){
        counter = counter + 1;
        if(counter > 100){
            GreenfootImage prince = new GreenfootImage("prince.png");
            frog.setImage(prince);
        }
    }
}
