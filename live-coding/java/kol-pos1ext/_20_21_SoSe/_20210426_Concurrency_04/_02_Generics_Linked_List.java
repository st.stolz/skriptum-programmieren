package _20_21_SoSe._20210426_Concurrency_04;

public class _02_Generics_Linked_List {
    public static void main(String[] args) {
        MyLinkedList<String> myList = new MyLinkedList<>();
        myList.add("Node 1");
        myList.add("Node 2");
        myList.add("Node 3");
        myList.add("Node 4");
        System.out.println(myList.toString());
    }
}

class MyLinkedList<D> {

    public Node<D> head;

    public void add(D data){

        Node<D> newNode = new Node<>(data);

        if(head == null){
            head = newNode;
        }
        else {
            Node<D> lastNode = lastNode();
            lastNode.nextNode = newNode;
        }

    }

    public Node<D> lastNode() {
        Node<D> lastNode = head;
        while(lastNode.nextNode != null){
            lastNode = lastNode.nextNode;
        }
        return lastNode;
    }

    @Override
    public String toString(){
        String returnString = "";

        Node<D> currentNode = head;

        while(currentNode != null){
            returnString += currentNode.content;
            returnString += "\n";
            currentNode = currentNode.nextNode;
        }

        return returnString;
    }

}


class Node<D> {
    public D content;
    public Node<D> nextNode;

    public Node(D content) {
        this.content = content;
    }    
}