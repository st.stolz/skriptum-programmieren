package _20_21_SoSe._20210301_Concurrency_01_Basics;

public class _01_Runnable {
    public static void main(String[] args) {
        // Wir befinden uns von Anfang an im main Thread
        System.out.printf("Starting Thread Name: %s %n",Thread.currentThread().getName());
        Job job = new Job(20);
        Thread thread1 = new Thread(job, "T1");
        // Achtung staret keinen neuen Thread!
        // thread.run();
        thread1.start();
        // hier laufen nun 2 Threads, weil ja bereits main läuft
        
        // wirft Exception: 
        // thread1.start();

        Thread thread2 = new Thread(job, "T2");
        thread2.start();
    }
}

class Job implements Runnable {

    int runs;

    public Job(int runs) {
        this.runs = runs;
    }

    @Override
    public void run() {

        for (int i = 0; i < runs; i++) {

            System.out.printf("%03d von %s %n", i, Thread.currentThread().getName());

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            
        }

    }
    
}