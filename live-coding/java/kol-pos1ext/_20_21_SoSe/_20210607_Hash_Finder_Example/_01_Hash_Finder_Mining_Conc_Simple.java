package _20_21_SoSe._20210607_Hash_Finder_Example;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;

public class _01_Hash_Finder_Mining_Conc_Simple{

    final static String blockDaten = "13fdsfs";
    public static void main(String[] args) {

        final int THREADS_N = 6;
        LinkedList<Thread> threadList = new LinkedList<>();

        long startTime = System.currentTimeMillis();

        for (int i = 0; i < THREADS_N; i++) {
            final int finalI = i;
            Runnable run = () -> {
                Miner.mine(blockDaten, "000000", Long.MAX_VALUE / THREADS_N * finalI);
            };
            Thread thread = new Thread(run,"Thread"+Integer.toString(i));
            threadList.add(thread);
            thread.start();
        }

        for (Thread thread : threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        long endTime = System.currentTimeMillis();

        System.out.printf("Execution Time: %d %n",endTime-startTime);
        
        
    }
}

class MiningData {
    private static boolean found = false;
    private static String hash;
    public static boolean isFound() {
        return found;
    }
    public synchronized static void setFound(boolean found) {
        MiningData.found = found;
    }
    public synchronized static String getHash() {
        return hash;
    }
    public synchronized static void setHash(String hash) {
        MiningData.hash = hash;
    }     
}

class Miner {
    public static boolean mine(String blockDaten, String difficulty, long startingNonce){
        boolean hashFound = false;

        long nonce = startingNonce;

        while(!MiningData.isFound() && nonce < Long.MAX_VALUE){

            String zuHashendeDaten = blockDaten + nonce;

            String hash = Hash.calculate(zuHashendeDaten);

            // System.out.printf("%s: %s %n",Thread.currentThread().getName(),hash);

            if(hash.startsWith(difficulty)){
                hashFound=true;
                MiningData.setFound(true);
                MiningData.setHash(hash);                
                System.out.printf("%s found Hash with %d tries %n",Thread.currentThread().getName(),nonce-startingNonce);
            }

            nonce++;

        }        

        if(!hashFound)
            System.out.printf("%s: exiting %n",Thread.currentThread().getName());

        return hashFound;
    }
}

class Hash {
    public static String calculate(String strToHash) {

        byte[] bytesOfMessage;
        try {
            bytesOfMessage = strToHash.getBytes("UTF-8");        
            MessageDigest md;        
            md = MessageDigest.getInstance("SHA-256");        
        
            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                String hex = Integer.toHexString(0xff & thedigest[i]);
                if(hex.length() == 1) 
                        hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } 
        catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        
    }
}