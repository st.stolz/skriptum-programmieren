package _20_21_SoSe._20210315_Concurrency_02_RaceC_basic;

import java.util.LinkedList;

public class _03_Race_Synchronize {

    public static void main(String[] args) {

        LinkedList<Thread> tList = new LinkedList<>();

        final int THREADS_N = 10;
        
        Runnable runner = new Runner();
        for (int i = 0; i < THREADS_N; i++) {
            // So würde synchronized Methode nichts bringen:
            // Runnable runner = new Runner();
            Thread t = new Thread(runner);
            t.start();
            tList.add(t);
        }

        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.printf("Endstand Counter: %d %n", Storage.counter);

    }
}

class Runner implements Runnable {

    final int COUNT_TIMES = 1000;

    @Override
    public void run() {
        for (int i = 0; i < COUNT_TIMES; i++) {
            count();
        }     
    }

    synchronized void count(){        
            Storage.counter ++;          
    }
    
}