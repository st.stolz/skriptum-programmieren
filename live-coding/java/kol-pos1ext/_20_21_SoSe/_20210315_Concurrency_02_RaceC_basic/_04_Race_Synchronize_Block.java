package _20_21_SoSe._20210315_Concurrency_02_RaceC_basic;

import java.util.LinkedList;

public class _04_Race_Synchronize_Block {

    public static void main(String[] args) {

        LinkedList<Thread> tList = new LinkedList<>();

        final int THREADS_N = 10;

        Object syncObject = new Object();
        
        for (int i = 0; i < THREADS_N; i++) {
            Runnable runner = new Runner2(syncObject);
            Thread t = new Thread(runner);
            t.start();
            tList.add(t);
        }

        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }    

        System.out.printf("Endstand Counter: %d %n", Storage.counter);

    }
}

class Runner2 implements Runnable {

    final int COUNT_TIMES = 1000;

    Object syncObject;

    public Runner2(Object syncObject){
        this.syncObject = syncObject;
    }

    @Override
    public void run() {
        for (int i = 0; i < COUNT_TIMES; i++) {
            synchronized(syncObject){
                Storage.counter ++;          
            }        
        }     
    }
    
}