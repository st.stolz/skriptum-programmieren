package _20_21_SoSe._20210315_Concurrency_02_RaceC_basic;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class _05_Hash_Finder_Mining{

    final static String blockDaten = "13fdsfs";
    public static void main(String[] args) {

        Miner.mine(blockDaten, "000000");
        
    }
}

class Miner {
    public static boolean mine(String blockDaten, String difficulty){
        boolean hashFound = false;

        long nonce = 0;

        while(!hashFound && nonce < Long.MAX_VALUE){

            String zuHashendeDaten = blockDaten + nonce;

            String hash = Hash.calculate(zuHashendeDaten);

            // System.out.println(hash);

            if(hash.startsWith(difficulty)){
                System.out.println(hash);
                hashFound = true;
                System.out.printf("Hash found with tries: %d %n",nonce);
            }

            nonce++;

        }        

        return hashFound;
    }
}

class Hash {
    public static String calculate(String strToHash) {

        byte[] bytesOfMessage;
        try {
            bytesOfMessage = strToHash.getBytes("UTF-8");        
            MessageDigest md;        
            md = MessageDigest.getInstance("SHA-256");        
        
            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                String hex = Integer.toHexString(0xff & thedigest[i]);
                if(hex.length() == 1) 
                        hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } 
        catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        
    }
}