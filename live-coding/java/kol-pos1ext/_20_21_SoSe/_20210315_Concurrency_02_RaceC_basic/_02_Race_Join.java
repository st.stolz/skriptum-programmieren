package _20_21_SoSe._20210315_Concurrency_02_RaceC_basic;

import java.util.LinkedList;

public class _02_Race_Join {

    public static void main(String[] args) {

        final int COUNT_TIMES = 1000;

        Runnable runner = () -> {
            for (int i = 0; i < COUNT_TIMES; i++) {
                Storage.counter ++;
            }
        };

        LinkedList<Thread> tList = new LinkedList<>();

        final int THREADS_N = 10;
        
        for (int i = 0; i < THREADS_N; i++) {
            Thread t = new Thread(runner);
            t.start();
            tList.add(t);
        }

        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.printf("Endstand Counter: %d %n", Storage.counter);

    }
}