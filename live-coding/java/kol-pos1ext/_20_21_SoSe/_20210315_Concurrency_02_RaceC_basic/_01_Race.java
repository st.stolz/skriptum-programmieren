package _20_21_SoSe._20210315_Concurrency_02_RaceC_basic;

public class _01_Race {

    public static void main(String[] args) {

        final int COUNT_TIMES = 1000;

        Runnable runner = () -> {
            for (int i = 0; i < COUNT_TIMES; i++) {
                Storage.counter ++;
            }
        };

        final int THREADS_N = 10;
        
        for (int i = 0; i < THREADS_N; i++) {
            Thread t = new Thread(runner);
            t.start();
        }

        System.out.printf("Endstand Counter: %d %n", Storage.counter);

    }
}

class Storage {
    public static long counter;
}