package _20_21_SoSe._20210412_Concurrency_03;

public class _03_WAIT_NOTIFY {
    public static void main(String[] args) {

        DataShareWait ds = new DataShareWait();

        Runnable sender = () -> {
            for (int i = 0; i < 1000; i++) {
                ds.setDataString("Message: " + Integer.toString(i));
            }
        };

        Runnable receiver = () -> {
            for (int i = 0; i < 1000; i++) {
                ds.getDataString();
            }
        };

        new Thread(sender).start();
        new Thread(receiver).start();
        
    }
}

class DataShareWait {

    private String dataString;
    boolean isSet = false;

    synchronized public String getDataString(){
        while(!isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }   
        String returnVal = dataString;
        System.out.println(returnVal);
        isSet = false;
        notify();
        return returnVal;
    }

    synchronized public void setDataString(String dataString){
        while(isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }        
        this.dataString = dataString;
        isSet = true;
        notify();
    }
}