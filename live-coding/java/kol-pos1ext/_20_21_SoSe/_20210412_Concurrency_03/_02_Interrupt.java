package _20_21_SoSe._20210412_Concurrency_03;

public class _02_Interrupt {

    public static void main(String[] args) {
        
        Runnable runner = () -> {
            int counter = 0;
            while(counter < 10 && !Thread.currentThread().isInterrupted()){
                System.out.printf("Durchgang: %d %n", counter+1);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.printf("Thread %s got interrupt Signal %n", Thread.currentThread().getName());
                    // Es muss noch einmal aktiv interrupt aufgerufen werden, weil der Interrupt zurückgesetzt wird durch die Exception
                    Thread.currentThread().interrupt();
                }
                counter ++;
            }
            // wäre nicht möglich, würde man den Thread einfach killen
            System.out.printf("stopping Thread: %s %n", Thread.currentThread().getName());
        };

        Thread t = new Thread(runner,"counter");
        t.start();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t.interrupt();

        System.out.printf("stopping Thread: %s %n", Thread.currentThread().getName());

    }
    
}