package _20_21_SoSe._20210412_Concurrency_03;

import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;

public class _01_Lock {
    public static void main(String[] args) {

        LinkedList<Thread> tList = new LinkedList<>();

        final int THREADS_N = 10;

        ReentrantLock lock = new ReentrantLock();
        
        for (int i = 0; i < THREADS_N; i++) {
            Runnable runner = new Runner2(lock);
            Thread t = new Thread(runner);
            t.start();
            tList.add(t);
        }

        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }    

        System.out.printf("Endstand Counter: %d %n", Storage.counter);

    }
}

class Runner2 implements Runnable {

    final int COUNT_TIMES = 1000;

    ReentrantLock lock;

    public Runner2(ReentrantLock lock){
        this.lock = lock;
    }

    @Override
    public void run() {
        for (int i = 0; i < COUNT_TIMES; i++) {
            lock.lock();
                Storage.counter ++;          
            lock.unlock();       
        }     
    }
    
}

class Storage {
    public static long counter;
}