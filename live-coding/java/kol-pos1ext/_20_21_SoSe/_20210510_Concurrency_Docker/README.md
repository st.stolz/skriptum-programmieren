# Inhalt

## Deadlocks

## Docker Containervirtualisierung für die Anwendungsentwicklung

1. [Docker Skriptum](https://gitlab.com/school-edu/betriebssysteme-skriptum/-/wikis/Serverdienste/Docker-Containervirtualisierung)
2. [Python Image](https://hub.docker.com/_/python)
3. [Jupyter Notebook](https://github.com/jupyter/docker-stacks)