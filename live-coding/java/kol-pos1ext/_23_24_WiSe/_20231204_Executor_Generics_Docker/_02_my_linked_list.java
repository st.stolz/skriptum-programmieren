package _23_24_WiSe._20231204_Executor_Generics_Docker;

public class _02_my_linked_list {

    public static void main(String[] args) {
        System.out.println("Starte belegen der Liste");
        MyLinkedListV1<String> myList = new MyLinkedListV1<>();
        myList.add("String 1");
        myList.add("String 2");
        myList.add("String 3");
        System.out.println("Printe Liste");
        System.out.println(myList);
    }
    
}

class MyLinkedListV1<M> {

    public ElementContainer<M> head;

    public void add(M data){
        ElementContainer<M> newElement = new ElementContainer<>(data);
        if(head == null){
            head = newElement;
        }
        else {
            ElementContainer<M> lastElement = this.lastElement();
            lastElement.nextElement = newElement;
        }
    }

    private ElementContainer<M> lastElement() {
        ElementContainer<M> lastElement = head;
        if(lastElement != null){
            while(lastElement.nextElement != null){
                lastElement = lastElement.nextElement;
            }
        }
        return lastElement;
    }

    @Override
    public String toString(){
        String returnStr = "";
        ElementContainer<M> currentElement = head;
        int counter = 0;
        while(currentElement!=null){
            returnStr += String.format("Element %d: %s %n", counter, currentElement.data);
            currentElement = currentElement.nextElement;
            counter++;
        }
        return returnStr;
    }


}


class ElementContainer<D> {
    public D data;
    public ElementContainer<D> nextElement;

    public ElementContainer(D data){
        this.data = data;
    }
}