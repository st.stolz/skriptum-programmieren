package _23_24_WiSe._20231204_Executor_Generics_Docker;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class _01_Executor_simple {
    public static void main(String[] args) {
        
        // Achtung bei Cached können unter Umständen zu viele Threads gestartet werden (kein Maximum)
        // ExecutorService exec = Executors.newCachedThreadPool();
        // Bei fixed werden die gestarteten Threads nicht mehr beendet (kein ttl)
        ExecutorService exec = Executors.newFixedThreadPool(3);
        // Meist wird Verwendung von ThreadPoolExecutor Constructor vorzuziehen sein

        int counter = 0;

        while(true){

            System.out.printf("%d Threads laufen gerade %n", Thread.activeCount());

            if(counter < 10){
                Job run = new Job(5, "Job-"+(counter+1));
                exec.execute(run);
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            counter++;
        }
    }
}

class Job implements Runnable {
    int runs;
    String name;

    public Job(int runs, String name){
        this.runs = runs;
        this.name = name;
    }

    @Override
    public void run() {       
        
        System.out.printf("* Starte Thread %s %n", name);
        for(int i = 0; i < runs; i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.printf("* STOPPE Thread %s %n", name);
    }
}
