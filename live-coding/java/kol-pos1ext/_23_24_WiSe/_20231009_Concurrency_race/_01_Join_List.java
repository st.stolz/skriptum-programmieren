package _23_24_WiSe._20231009_Concurrency_race;

import java.util.ArrayList;
import java.util.List;

public class _01_Join_List {
    public static void main(String[] args) {

        Runnable job_counting = () -> {
            System.out.print(Thread.currentThread().getName());
            System.out.println(" ... count job starting!");

            int nTimes = 1000;

            for (int i = 0; i < nTimes; i++) {
                Storage.counter = Storage.counter + 1;
            }            

            System.out.println(" ... count job stopping!");
        };

        int nThreads = 10;
        List<Thread> tList = new ArrayList<>();

        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread(job_counting);
            tList.add(t);
            t.start();
        }

        // Trotz join() auf alle Threads wird immer noch nicht bis 10000 gezählt
        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        System.out.println("Counting done: "+Storage.counter);
    }
}

class Storage {
    public static long counter = 0;
}
