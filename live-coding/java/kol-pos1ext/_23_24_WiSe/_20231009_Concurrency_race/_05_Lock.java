package _23_24_WiSe._20231009_Concurrency_race;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class _05_Lock {
    public static void main(String[] args) {
        

        int nThreads = 10;
        List<Thread> tList = new ArrayList<>();

        ReentrantLock lock = new ReentrantLock();
        

        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread(new Job_Counting4(lock));
            tList.add(t);
            t.start();
        }

        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        System.out.println("Counting done: "+Storage5.counter);
    }
}

class Storage5 {
    public static long counter = 0;
}

class Job_Counting4 implements Runnable {

    ReentrantLock lock;

    public Job_Counting4(ReentrantLock lock){
        this.lock = lock;
    }

    @Override
    public void run() {        
        System.out.print(Thread.currentThread().getName());
        System.out.println(" ... count job starting!");

        int nTimes = 1000;

        for (int i = 0; i < nTimes; i++) {
            lock.lock();
            Storage5.counter = Storage5.counter + 1;
            lock.unlock();      
        }            

        System.out.println(" ... count job stopping!");
    }
    
}
