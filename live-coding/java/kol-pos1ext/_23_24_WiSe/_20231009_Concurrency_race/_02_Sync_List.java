package _23_24_WiSe._20231009_Concurrency_race;

import java.util.ArrayList;
import java.util.List;

public class _02_Sync_List {
    public static void main(String[] args) {
        

        int nThreads = 10;
        List<Thread> tList = new ArrayList<>();

        Runnable job = new Job_Counting();

        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread(job);
            tList.add(t);
            t.start();
        }

        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        System.out.println("Counting done: "+Storage2.counter);
    }
}

class Storage2 {
    public static long counter = 0;
}

class Job_Counting implements Runnable {

    @Override
    public void run() {        
        System.out.print(Thread.currentThread().getName());
        System.out.println(" ... count job starting!");

        int nTimes = 1000;

        for (int i = 0; i < nTimes; i++) {
            countJob();
        }            

        System.out.println(" ... count job stopping!");
    }

    synchronized void countJob(){
        // wichtig: Immer nur synchronized was nicht thread safe ist
        Storage2.counter = Storage2.counter + 1;
    }
    
}
