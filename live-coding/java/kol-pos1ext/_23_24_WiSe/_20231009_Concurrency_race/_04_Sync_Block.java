package _23_24_WiSe._20231009_Concurrency_race;

import java.util.ArrayList;
import java.util.List;

public class _04_Sync_Block {
    public static void main(String[] args) {
        

        int nThreads = 10;
        List<Thread> tList = new ArrayList<>();

        Object syncObject = new Object();
        

        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread(new Job_Counting3(syncObject));
            tList.add(t);
            t.start();
        }

        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        System.out.println("Counting done: "+Storage4.counter);
    }
}

class Storage4 {
    public static long counter = 0;
}

class Job_Counting3 implements Runnable {

    Object syncObject;

    public Job_Counting3(Object syncObject){
        this.syncObject = syncObject;
    }

    @Override
    public void run() {        
        System.out.print(Thread.currentThread().getName());
        System.out.println(" ... count job starting!");

        int nTimes = 1000;

        for (int i = 0; i < nTimes; i++) {
            synchronized(syncObject){
                Storage4.counter = Storage4.counter + 1;
            }            
        }            

        System.out.println(" ... count job stopping!");
    }
    
}
