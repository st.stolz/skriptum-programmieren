package _23_24_WiSe._20231009_Concurrency_race;

import java.util.ArrayList;
import java.util.List;

public class _03_Sync_List_false {
    public static void main(String[] args) {
        

        int nThreads = 10;
        List<Thread> tList = new ArrayList<>();

        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread(new Job_Counting2());
            tList.add(t);
            t.start();
        }

        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        System.out.println("Counting done: "+Storage3.counter);
    }
}

class Storage3 {
    public static long counter = 0;
}

class Job_Counting2 implements Runnable {

    @Override
    public void run() {        
        System.out.print(Thread.currentThread().getName());
        System.out.println(" ... count job starting!");

        int nTimes = 1000;

        for (int i = 0; i < nTimes; i++) {
            countJob();
        }            

        System.out.println(" ... count job stopping!");
    }

    // wird jetzt nicht mehr funktionieren, weil synchronized pro Objekt wirkt und nicht pro Klasse
    synchronized void countJob(){
        Storage3.counter = Storage3.counter + 1;
    }
    
}
