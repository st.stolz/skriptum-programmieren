package _23_24_WiSe._20231023_Concurrency_locks_interrupt;

public class _03_interrupts_basic {
    public static void main(String[] args) {
        Runnable runner = () -> {

            while(!Thread.currentThread().isInterrupted()){
                System.out.println("I am running!");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("Shutting down!");
                    Thread.currentThread().interrupt();
                }
            }

        };

        Thread t1 = new Thread(runner);
        t1.start();

        try {
            Thread.sleep(5 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t1.interrupt();
    }
}
