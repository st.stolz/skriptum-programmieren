package _23_24_WiSe._20231023_Concurrency_locks_interrupt;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class _01_dead_lock {

    static Lock lock1 = new ReentrantLock();
    static Lock lock2 = new ReentrantLock();

    public static void main(String[] args) {
        
        Runnable run1 = () -> {
            while (true){
                lock1.lock();
                // System.out.printf("%s has lock1 taken %n", Thread.currentThread().getName());
                lock2.lock();
                // System.out.printf("%s has lock2 taken %n", Thread.currentThread().getName());

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // System.out.printf("%s runs %n", Thread.currentThread().getName());    
                System.out.println("Run1");                       
                
                lock1.unlock();
                lock2.unlock();
            }
        };

        Runnable run2 = () -> {
            while (true){
                lock2.lock();
                // System.out.printf("%s has lock2 taken %n", Thread.currentThread().getName());  
                lock1.lock();
                // System.out.printf("%s has lock1 taken %n", Thread.currentThread().getName());

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // System.out.printf("%s runs %n", Thread.currentThread().getName());
                System.out.println("Run2");  
                
                lock2.unlock();
                lock1.unlock();
            }
        };

        new Thread(run2, "run2").start();
        new Thread(run1, "run1").start();
        
    }
}
