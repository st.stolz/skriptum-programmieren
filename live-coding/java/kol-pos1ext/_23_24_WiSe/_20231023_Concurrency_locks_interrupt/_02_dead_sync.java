package _23_24_WiSe._20231023_Concurrency_locks_interrupt;

public class _02_dead_sync {
    public static void main(String[] args) {

        Object syncObj1 = new Object();
        Object syncObj2 = new Object();

        Runnable run1 = () -> {
            while (true){

                synchronized(syncObj1){
                    synchronized(syncObj2){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        // System.out.printf("%s runs %n", Thread.currentThread().getName());    
                        System.out.println("Run1");     
                            }
                }
                
            }
        };

        Runnable run2 = () -> {
            while (true){

                synchronized(syncObj2){
                    synchronized(syncObj1){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        // System.out.printf("%s runs %n", Thread.currentThread().getName());    
                        System.out.println("Run2");     
                            }
                }
                
            }
        };

        new Thread(run2, "run2").start();
        new Thread(run1, "run1").start();
    }
}
