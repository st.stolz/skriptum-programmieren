package _23_24_WiSe._20231106_Hash_Finder_wait_notify_queque;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class _01_hash_finder {

    public static void main(String[] args) {
        HashFinder.findHash("T1,T2,T33", "000000");
    }
    
}

class HashFinder {
    

    public static boolean findHash(String transactionData, String difficulty){

        boolean isFound = false;
        // long nonce = Long.MIN_VALUE;
        long nonce = 0;

        while(!isFound && nonce <= Long.MAX_VALUE){
            String strToHash = transactionData + nonce;
            String hash = calculateHash(strToHash);
            //System.out.printf("Try with nonce %d from Thread %s - %s %n", nonce, Thread.currentThread(),hash);
            if(hash.startsWith(difficulty)){
                isFound = true;
                System.out.printf("Found with nonce %d from Thread %s - %s %n", nonce, Thread.currentThread(),hash);
            }
            nonce++;
        }

        return isFound;

    }

    public static String calculateHash(String strToHash) {
        byte[] bytesOfMessage;
        try {
            bytesOfMessage = strToHash.getBytes("UTF-8");        
            MessageDigest md;        
            md = MessageDigest.getInstance("SHA-256");        
        
            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                String hex = Integer.toHexString(0xff & thedigest[i]);
                if(hex.length() == 1) 
                        hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } 
        catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        
    }

}
