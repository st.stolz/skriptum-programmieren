package _23_24_WiSe._20231106_Hash_Finder_wait_notify_queque;

public class _02_wait_notify {
    public static void main(String[] args) {
        DataShareWait ds = new DataShareWait();

        Runnable sender = () -> {
            for (int i = 0; i < 100; i++) {
                ds.setDataString("Message: " + i);
            }
        };

        Runnable receiver = () -> {
            for (int i = 0; i < 100; i++) {
                String str = ds.getDataString();
                System.out.println(str);
            }
        };

        new Thread(sender).start();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(receiver).start();
    }

}

class DataShareWait {
    volatile private String dataString;
    volatile boolean isSet = false;

    synchronized public String getDataString() {
        while(!isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String returnVal = this.dataString;

        isSet = false;

        notify();

        return returnVal;
    }

    synchronized public void setDataString(String message) {
        while(isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }        

        this.dataString = message;

        System.out.println("Message sent!");

        isSet = true;
        notify();
    }

}
