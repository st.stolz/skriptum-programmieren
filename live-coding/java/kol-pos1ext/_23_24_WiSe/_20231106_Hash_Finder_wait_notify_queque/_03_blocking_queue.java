package _23_24_WiSe._20231106_Hash_Finder_wait_notify_queque;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class _03_blocking_queue {

    public static void main(String[] args) {
        DataShareQueue ds = new DataShareQueue();

        Runnable sender = () -> {
            for (int i = 0; i < 100; i++) {
                ds.setDataString("Message: " + i);
            }
        };

        Runnable receiver = () -> {
            for (int i = 0; i < 100; i++) {
                String str = ds.getDataString();
                System.out.println(str);
            }
        };

        new Thread(sender).start();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(receiver).start();
    }
}

class DataShareQueue {
    private BlockingQueue<String> q = new ArrayBlockingQueue<>(10);

    public String getDataString() {
        String returnVal = "";

        try {
            returnVal = q.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return returnVal;
    }

    public void setDataString(String message) {

        try {
            q.put(message);
            System.out.println("Message sent!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}