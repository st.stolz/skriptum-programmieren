package _23_24_WiSe._20230925_Concurrency;

public class _01_Concurrency_Thread_Runnable {

    public static void main(String[] args) {
        System.out.println("Hello World");
        // Name main Thread:
        System.out.println(Thread.currentThread().getName());
        Thread t1 = new Thread(new Job());
        // hier wird kein neuer Thread gestartet
        t1.run();
        // So starten wir einen neuen Thread
        t1.start();
        Thread t2 = new Thread(new Job());
        t2.start();
        // java.lang.IllegalThreadStateException
        // t2.start();
        Thread t3 = new Thread(new Job(), "Der vierte Thread");
        t3.start();
    }
    
}

class Job implements Runnable {

    @Override
    public void run() {

        System.out.print(Thread.currentThread().getName());
        System.out.println(" ... Going to Sleep");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            System.out.println("InterruptedException occured");
        }
    
    }

}
