package _23_24_WiSe._20230925_Concurrency;

public class _02_Lamnda_Expression {
    public static void main(String[] args) {
        System.out.println("Hello World");

        Runnable job = () -> {
            System.out.print(Thread.currentThread().getName());
            System.out.println(" ... Going to Sleep");

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException occured");
            }
        };

        Thread t1 = new Thread(job);
        t1.start();
    }
}
