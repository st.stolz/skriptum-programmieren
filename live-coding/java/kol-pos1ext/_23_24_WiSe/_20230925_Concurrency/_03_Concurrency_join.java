package _23_24_WiSe._20230925_Concurrency;

public class _03_Concurrency_join {

    public static void main(String[] args) {
        Runnable job_long = () -> {
            System.out.print(Thread.currentThread().getName());
            System.out.println(" ... Long Thread Going to Sleep");
            // durch join() kann man auf einen Thread warten
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException occured");
            }

            System.out.println(" ... long job stopping!");
        };
        Runnable job_short = () -> {
            System.out.print(Thread.currentThread().getName());
            System.out.println(" ... Short Thread Going to Sleep");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException occured");
            }

            System.out.println(" ... short job stopping!");
        };
        // obwohl t1 vor t2 gestartet wird, kann es sein, dass der Scheduler t2 zuerst Rechenzeit gibt
        // TODO starte in einer Schleife eine beliebige Anzahl Threads und warte dann bis alle Threads beendet sind
        Thread t1 = new Thread(job_long);        
        t1.start();
        
        try {
            t1.join();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException occured");
        }
        Thread t2 = new Thread(job_short);
        t2.start();
    }
    
}
