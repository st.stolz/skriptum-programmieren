package _20_21_WiSe._20201122_Concurrency_race_condition;

import java.util.LinkedList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class _02_Lock {
    public static void main(String[] args) {
        LinkedList<Thread> threads = new LinkedList<>();
        Object lock = new ReentrantLock();

        for (int i = 0; i < 100; i++) {
            Thread t = new Thread(new CounterJob(lock));
            t.start();
            threads.add(t);
        }

        // Mit Pause schafft er es _eher_ hochzuzählen
        // try {
        // Thread.sleep(50);
        // } catch (InterruptedException e) {
        // e.printStackTrace();
        // }

        // Mit join() kann man auf alle Threads warten
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Wert kann aber trotzdem noch nicht stimmen wegen RaceCondition
        System.out.println(DataShare.counter);
    }
}

class CounterJobLock implements Runnable{

    Lock lock;

    public CounterJobLock(Lock lock){
        this.lock = lock;
    }

    // synchronized public void run() -> würde nichts bringen, weil mehrere CounterJob Objekte erstellt werden
    @Override
    public void run() {
        for (int i = 0; i < 1000000; i++) {
            lock.lock();
            DataShare.counter++;
            lock.unlock();                      
        }
    }
    
}