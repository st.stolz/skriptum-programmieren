package _20_21_WiSe._20201122_Concurrency_race_condition;

public class _03_wait_notify {

    public static void main(String[] args) {

        DataShareWait ds = new DataShareWait();

        Runnable sender = () -> {
            int counter = 0;
            while (counter < 1000) {
                counter++;
                ds.setDataString("Message: " + Integer.toString(counter));
            }
        };

        Runnable receiver = () -> {
            int counter = 0;
            while (counter < 1000) {
                ds.getDatastring();
                counter++;
            }
        };

        new Thread(sender).start();
        new Thread(receiver).start();

    }
}

class DataShareWait {

    private String dataString;
    boolean isSet = false;

    synchronized public String getDatastring() {
        while(!isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }        
        String returnVal = dataString;
        System.out.println(dataString);
        isSet = false;
        notify();
        return returnVal;
    }

    synchronized public void setDataString(String dataString) {
        while(isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }        
        this.dataString = dataString;
        isSet = true;
        notify();
    }

}