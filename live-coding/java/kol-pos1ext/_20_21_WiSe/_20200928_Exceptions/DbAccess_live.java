package _20_21_WiSe._20200928_Exceptions;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Connection;

public class DbAccess_live {
    public static void main(String[] args) {
        DbAccessLive db = new DbAccessLive();
        try {
            db.connect();
        } catch (ToManyAttemptsException e) {
            System.out.println(e.toString());
        }
    }
}

class DbAccessLive {
    Connection conn;
    String conURL = "jdbc:mysql://localhost:3306/w3c";
    final int tries = 3;

    public void connect() throws ToManyAttemptsException {
        int counter = 0;
        boolean connected = false;
        while (counter < tries && !connected) {
            try {
                conn = (Connection) DriverManager.getConnection(conURL, "root", "123");
                System.out.println("Connection established!");
                connected = true;
            } 
            catch(SQLTimeoutException e){

            }
            
            catch (SQLException e) {
                System.out.println("Exception!");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
            finally{
                counter++;
            }
        }
        if(!(counter < tries)){
            throw new ToManyAttemptsException(tries);
        }
    }
}

class ToManyAttemptsException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    ToManyAttemptsException(int tries) {
        super(String.format("To many connection attempts. Tried %s times %n",tries));
    }
}
