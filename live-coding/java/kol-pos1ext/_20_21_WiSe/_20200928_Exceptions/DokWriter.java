package _20_21_WiSe._20200928_Exceptions;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DokWriter {

	public static void main(String[] args) {
		FileWorker.addNewVersion("Hello World");
	}

}

class FileWorker
{
    private static String FILE_NAME = "filename";
    public static void addNewVersion(String data) {
        int counter = 1;
        String filename = null;
        boolean running = true;
        while(running) {
            try {
            	filename = String.format(FILE_NAME+"_%03d",counter);
                Files.createFile(Paths.get(filename));
                running = false;
            }
            catch(FileAlreadyExistsException e) {
                running = true;
                System.err.printf("Datei umbenannt in: %s%n",filename);
            }
            catch(IOException e){
                System.err.printf("Konnte Datei %s nicht erstellen: %s%n",filename,e);
                System.exit(0);
            }    
            counter++;
        }        
        
        PrintWriter out = null;
        try {
            out = new PrintWriter(filename);
            out.println(data);
        }
        catch(IOException e) {
            System.err.printf("Konnte nicht in Datei %s schreiben: %s%n",filename,e);
        }
        finally {
            out.close();     
        }
    }
}
