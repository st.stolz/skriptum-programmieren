package _20_21_WiSe._20201109_Concurrency;

import java.util.LinkedList;

public class _03_Join {
    public static void main(String[] args) {
        LinkedList<Thread> threads = new LinkedList<>();

        for (int i = 0; i < 3; i++) {
            Thread t = new Thread(new CounterJob());
            t.start();
            threads.add(t);
        }

        // Mit Pause schafft er es _eher_ hochzuzählen
        // try {
        // Thread.sleep(50);
        // } catch (InterruptedException e) {
        // e.printStackTrace();
        // }

        // Mit join() kann man auf alle Threads warten
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Wert kann aber trotzdem noch nicht stimmen wegen RaceCondition
        System.out.println(DataShare.counter);
    }
}

class CounterJob implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 1000000; i++) {
            DataShare.counter++;
        }
    }
    
}

class DataShare {
    public static int counter = 0;
}