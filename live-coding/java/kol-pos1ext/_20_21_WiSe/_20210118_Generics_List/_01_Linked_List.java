package _20_21_WiSe._20210118_Generics_List;

public class _01_Linked_List {
    public static void main(String[] args) {
        MyLinkedList<String> myList = new MyLinkedList<>();
        myList.add("Element 1");
        myList.add("Element 2");
        myList.add("Element 3");
        System.out.println(myList.toString());
    }
    
    
}

class MyLinkedList<D> {
    public Element<D> firstElement;

    public void add(D data){
        Element<D> newElement = new Element<>(data);
        if(firstElement == null)
            firstElement = newElement;
        else {
            Element<D> lastElement = lastElement();
            lastElement.nextElement = newElement;
        }
    }

    public Element<D> lastElement(){
        Element<D> currentElement = firstElement;
        while(currentElement.nextElement != null){
            currentElement = currentElement.nextElement;
        }
        return currentElement;
    }

    @Override
    public String toString(){
        String returnString = "";
        Element<D> currentElement = firstElement;
        do {
            returnString += currentElement.data;
            returnString += "\n";
            currentElement = currentElement.nextElement;
        }while(currentElement != null);
        return returnString;
    }

}

class Element<D> {
    public D data;
    public Element<D> nextElement;

    public Element(D data){
        this.data = data;
    }
}