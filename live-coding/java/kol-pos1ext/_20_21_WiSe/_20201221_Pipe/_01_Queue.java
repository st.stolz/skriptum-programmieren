package _20_21_WiSe._20201221_Pipe;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class _01_Queue {

    public static void main(String[] args) {
        DataShare01 ds = new DataShare01();

        Runnable sender = () -> {
            
            for (int i = 0; i < 10; i++) {    
                System.out.println("In Loop");            
                ds.setDataString("Message: "+i);
            }
        };

        Runnable receiver = () -> {
            for (int i = 0; i < 10; i++) {
                ds.getDataString();
            }
        };

        Thread t1 = new Thread(sender);
        Thread t2 = new Thread(receiver);
        t1.start();
        t2.start();

    }

}

class DataShare01 {

    private BlockingQueue<String> q = new ArrayBlockingQueue<>(1);

    public String getDataString() {
        System.out.println("Get Data String");
        String dataString = "";
        try {
            dataString = q.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Got: " + dataString);
        return dataString;
    }

    public void setDataString(String str) {
        System.out.println("Set Data String");
        try {
            q.put(str);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Data String was set");
    }


}
