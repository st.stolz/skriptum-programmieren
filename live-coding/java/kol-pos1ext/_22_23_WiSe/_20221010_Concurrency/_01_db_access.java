package _22_23_WiSe._20221010_Concurrency;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class _01_db_access {

    public static void main(String[] args) {
        try {
            DbWork.connect();
        } catch (DatabaseNotAvailableException e) {
            e.printStackTrace();
        }
    }
    
}

class DbWork {

    static Connection conn = null;
    static Statement stmt = null;

    public static void connect() throws DatabaseNotAvailableException {
        boolean connected = false;
        final int tries = 5;
        int counter = 0;

        while(!connected && counter < tries){

            try {
                // TODO Erweitere an dieser Stelle den Code, sodass die Datenbank weg gelassen wird (w3c)
                // TODO Statdessen wird in einem zweiten Schritt mit SQL "use w3c;" geprüft, ob sie vorhanden ist
                // Auf diese Weise kann man unterscheiden, ob der Server noch nicht gestartet ist oder die Datenbank nicht erstellt wurde
                // Existiert nur die Datenbank nicht, so erstelle sie mit einem weiteren SQL Befehl
                /**
                 * Tipp: Holt euch mit createStatement() ein Statement aus dem Connection Objekt
                 * dort gibt es dann die Methode executeUpdate(String sql), um SQL auszuführen
                 */
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/w3c","root","123");
                System.out.println("Connection created!");
                connected = true;
            } 

            // catch (SQLTimeoutException e){
            //     System.out.println("Timeout!");                
            // }
            
            catch (SQLException e) {
                System.out.println("Database connection not possible. Trying again....");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
            counter++;
        }

        if(counter>=tries)
            throw new DatabaseNotAvailableException(tries);
    }
}

class DatabaseNotAvailableException extends Exception {
    DatabaseNotAvailableException(int tries){
        super(String.format("Stopped connecting after %d tries %n",tries));
    }
}