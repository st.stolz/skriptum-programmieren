package _22_23_WiSe._20221107_Concurrency_interrupt_deadlock_hashfinder;

public class _04_Interrupts {

    public static void main(String[] args) {
        
        Runnable runner = () -> {

            while(!Thread.currentThread().isInterrupted()){
                System.out.println("I am running!");        
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("Shutting down!");
                    Thread.currentThread().interrupt();
                }        
            }

        };

        Thread t1 = new Thread(runner);
        t1.start();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t1.interrupt();

    }
    
}
