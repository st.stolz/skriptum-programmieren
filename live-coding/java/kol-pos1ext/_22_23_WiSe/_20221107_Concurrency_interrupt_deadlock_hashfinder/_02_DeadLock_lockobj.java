package _22_23_WiSe._20221107_Concurrency_interrupt_deadlock_hashfinder;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class _02_DeadLock_lockobj {

    static Lock lock1 = new ReentrantLock();
    static Lock lock2 = new ReentrantLock();

    public static void main(String[] args) {

        Runnable run1 = () -> {

            while(true){
                lock1.lock();
                lock2.lock();

                System.out.printf("%s runs %n",Thread.currentThread().getName());

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                lock1.unlock();
                lock2.unlock();
            }

        };

        Runnable run2 = () -> {

            while(true){
                lock2.lock();
                lock1.lock();                

                System.out.printf("%s runs %n",Thread.currentThread().getName());

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                lock1.unlock();
                lock2.unlock();
            }

        };

        new Thread(run1, "run1").start();
        new Thread(run2, "run2").start();
        
    }
    
}
