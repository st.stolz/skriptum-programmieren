package _22_23_WiSe._20221107_Concurrency_interrupt_deadlock_hashfinder;



public class _01_Runnable_test {

    public static void main(String[] args) {
    
        Thread t1 = new Thread(new Runner());
    
        final int THREADS_N = 10;
    
        for (int i = 0; i < THREADS_N; i++) {
            t1.start();
        }
    
    }
}
    
class Runner implements Runnable {
    
    @Override
    public void run() {
        System.out.println("Hello World!");
    }
    
}


