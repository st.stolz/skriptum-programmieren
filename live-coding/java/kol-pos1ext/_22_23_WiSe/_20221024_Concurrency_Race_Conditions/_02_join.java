package _22_23_WiSe._20221024_Concurrency_Race_Conditions;

import java.util.LinkedList;

public class _02_join {
    public static void main(String[] args) {
        
        String threadName = Thread.currentThread().getName();
        System.out.printf("Starting %s Thread %n",threadName);

        final int COUNT_TIMES = 10000;
        final int THREADS_N = 10;

        Runnable runner = () -> {
            for (int i = 0; i < COUNT_TIMES; i++) {
                Storage.counter ++;
            }
        };

        LinkedList<Thread> tList = new LinkedList<>();

        for (int i = 0; i < THREADS_N; i++) {
            Thread t = new Thread(runner);
            t.start();
            tList.add(t);
        }

        for (Thread t : tList) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }            
        }        

        System.out.printf("Endstand Counter: %d %n",Storage.counter);

    }
}