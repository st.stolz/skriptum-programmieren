package _22_23_WiSe._20221024_Concurrency_Race_Conditions;

import java.util.LinkedList;

public class _07_synchronize_block {
    public static void main(String[] args) {
        
        String threadName = Thread.currentThread().getName();
        System.out.printf("Starting %s Thread %n",threadName);
        
        final int THREADS_N = 10;

        LinkedList<Thread> tList = new LinkedList<>();        
        Object syncObject = tList;

        for (int i = 0; i < THREADS_N; i++) {
            Runner5 runner = new Runner5(syncObject);
            Thread t = new Thread(runner);
            t.start();
            tList.add(t);
        }

        for (Thread t : tList) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }            
        }        

        System.out.printf("Endstand Counter: %d %n",Storage.counter);

    }
}

class Runner5 implements Runnable {
    final int COUNT_TIMES = 100000;
    Object syncObject;

    public Runner5(Object syncObject){
        this.syncObject = syncObject;
    }

    @Override
    public void run() {
        for (int i = 0; i < COUNT_TIMES; i++) {
            // Nun wird ein frei wählbares Objekt zum synchronisieren verwendet
            synchronized(syncObject){
                Storage.counter ++;
            }            
        }
        
    }

}