package _22_23_WiSe._20221024_Concurrency_Race_Conditions;

public class _01_counter {
    public static void main(String[] args) {
        
        String threadName = Thread.currentThread().getName();
        System.out.printf("Starting %s Thread %n",threadName);

        final int COUNT_TIMES = 10000;
        final int THREADS_N = 10;

        Runnable runner = () -> {
            for (int i = 0; i < COUNT_TIMES; i++) {
                Storage.counter ++;
            }
        };

        for (int i = 0; i < THREADS_N; i++) {
            Thread t = new Thread(runner);
            t.start();
        }

        System.out.printf("Endstand Counter: %d %n",Storage.counter);

    }
}

class Storage {
    public static long counter = 0;
}