package _22_23_WiSe._20221024_Concurrency_Race_Conditions;

import java.util.LinkedList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class _08_synchronize_lock_object {
    public static void main(String[] args) {
        
        String threadName = Thread.currentThread().getName();
        System.out.printf("Starting %s Thread %n",threadName);

        ReentrantLock lock = new ReentrantLock();
        
        final int THREADS_N = 10;

        LinkedList<Thread> tList = new LinkedList<>();

        for (int i = 0; i < THREADS_N; i++) {            
            Runner6 runner = new Runner6(lock);
            Thread t = new Thread(runner);
            t.start();
            tList.add(t);
        }

        for (Thread t : tList) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }            
        }        

        System.out.printf("Endstand Counter: %d %n",Storage.counter);

    }
}

class Runner6 implements Runnable {
    final int COUNT_TIMES = 100000;
    Lock lock;

    public Runner6(Lock lock){
        this.lock = lock;
    }

    @Override
    public void run() {
        for (int i = 0; i < COUNT_TIMES; i++) {       
            lock.lock();
            Storage.counter ++;      
            lock.unlock();              
        }
        
    }

}