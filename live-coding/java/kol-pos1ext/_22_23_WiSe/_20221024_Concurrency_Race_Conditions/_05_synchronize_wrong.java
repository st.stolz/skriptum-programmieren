package _22_23_WiSe._20221024_Concurrency_Race_Conditions;

import java.util.LinkedList;

public class _05_synchronize_wrong {
    public static void main(String[] args) {
        
        String threadName = Thread.currentThread().getName();
        System.out.printf("Starting %s Thread %n",threadName);
        
        final int THREADS_N = 10;

        LinkedList<Thread> tList = new LinkedList<>();
        
        for (int i = 0; i < THREADS_N; i++) {
            // synchronized synchronisiert auf das Runnable Objekt
            // Das heißt, wenn ein neues Runnable Objekt erstellt wird,
            // wird nicht zwischen den zwei verschiedenen Objekten synchronisiert
            Runner3 runner = new Runner3();
            Thread t = new Thread(runner);
            t.start();
            tList.add(t);
        }

        for (Thread t : tList) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }            
        }        

        System.out.printf("Endstand Counter: %d %n",Storage.counter);

    }
}

class Runner3 implements Runnable {
    final int COUNT_TIMES = 100000;

    @Override
    public void run() {
        for (int i = 0; i < COUNT_TIMES; i++) {
            count();
        }
        
    }

    // Wenn count() wie folgt implementiert wird, dann schaltet man die Nebenläufigkeit aus
    // synchronized public void count(){
    //     for (int i = 0; i < COUNT_TIMES; i++) {
    //         Storage.counter ++;
    //     }
    // }

    synchronized public void count(){
        Storage.counter ++;
    }

}