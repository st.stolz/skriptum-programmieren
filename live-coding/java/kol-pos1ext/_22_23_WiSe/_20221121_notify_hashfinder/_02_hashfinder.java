package _22_23_WiSe._20221121_notify_hashfinder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class _02_hashfinder {
    public static void main(String[] args) {

        String transactionData = "Hallo Stefan";

        HashFinder.findHash(transactionData, "0000000");
        
    }
}

class HashFinder {

    public static boolean findHash(String transactionData,String difficutly){

        boolean isFound = false;
        long nonce = 0;

        while(!isFound && nonce < Long.MAX_VALUE){
            String strToHash = transactionData+nonce;
            String hash = calculateHash(strToHash);
            // System.out.printf("Try with nonce %d from Thread %s - %s %n", nonce, Thread.currentThread().getName(), hash);
            if(hash.startsWith(difficutly)){
                isFound = true;
                System.out.printf("Found with nonce %d from Thread %s - %s %n", nonce, Thread.currentThread().getName(), hash);
            }
            nonce++;
        }


        return true;
    }
    /**
     * Calculates sha256 to a String
     * 
     * @param strToHash
     * @return
     */
    public static String calculateHash(String strToHash) {

        byte[] bytesOfMessage;
        try {
            bytesOfMessage = strToHash.getBytes("UTF-8");        
            MessageDigest md;        
            md = MessageDigest.getInstance("SHA-256");        
        
            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                String hex = Integer.toHexString(0xff & thedigest[i]);
                if(hex.length() == 1) 
                        hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } 
        catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        
    }
}
