package _22_23_WiSe._20221121_notify_hashfinder;

public class _01_wait_notify {
    public static void main(String[] args) {

        DataShareWait ds = new DataShareWait();

        Runnable sender = () -> {
            for (int i = 0; i < 1000; i++) {
                ds.setDataString("Message: "+Integer.toString(i));
                
                // try {
                //     Thread.sleep(1000);
                // } catch (InterruptedException e) {
                //     e.printStackTrace();
                // }
            }
        };

        Runnable receiver = () -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("Got Message: "+ds.getDataString());
            }
        };

        new Thread(sender).start();
        new Thread(receiver).start();
    }
}

class DataShareWait {

    private String dataString;
    boolean isSet = false;

    synchronized public String getDataString(){
        while(!isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String returnVal = dataString;

        isSet = false;

        notify();

        return returnVal;
    }

    synchronized public void setDataString(String message){
        while(isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        this.dataString = message;
        System.out.println("Message sent!");
        isSet = true;

        notify();
    }
}
