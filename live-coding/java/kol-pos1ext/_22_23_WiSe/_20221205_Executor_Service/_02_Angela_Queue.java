package _22_23_WiSe._20221205_Executor_Service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class _02_Angela_Queue {
    public static void main(String[] args) {

        DataShareWait ds = new DataShareWait();

        //Das ist der Sender. Er sendet unendlich viele Nachrichten und schreibt diese in den gemeinsamen 
        //Speicher (= die BlockingQueue) in der Methode setDataString()
        Runnable sender = () -> {
            for (int i = 0; i < 100; i++) {

                ds.setDataString("Message: "+Integer.toString(i));
                System.out.println("Message sent!");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        //Das ist der Empfänger. Er gibt die Nachricht aus, sobald er sie empfängt
        Runnable receiver = () -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("Got Message: "+ds.getDataString());
            }
        };

        new Thread(sender).start();
        new Thread(receiver).start();
    }
}

//Klasse DataShareWait ist die Klasse für die Kommunikation (= das Nachrichten versenden und ausgeben)
class DataShareWait {

    //Hier erstellen wir die BlockingQueue. Diese hat eine Capacity von 1, das bedeutet, dass man nur einen 
    //Wert (= nur eine Nachricht) reinspeichern kann. Wenn kein Speicherplatz ist, dann wartet der Code beim 
    //Sender bis wieder ein Platz ist, und wenn die Queue leer ist, dann wartet der Code beim Empfänger bis 
    //wieder ein Wert drin steht
    BlockingQueue<String> queue = new LinkedBlockingQueue<>(1);

    private String dataString;
    boolean isSet = false;

    synchronized public String getDataString(){
        String returnVal = dataString;
        try {
            //Hier versucht die message aus der Queue rauszuholen (Queue ist dann wieder leer). Das geht nur, 
            //wenn die Queue nicht leer ist. Sonst wird gewartet bis wieder ein Wert in der Queue drinsteht
            queue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return returnVal;
    }

    synchronized public void setDataString(String message){
        try {
            //Hier versucht man die message in die queue zu schreiben. Das geht nur, wenn die Queue leer ist, weil
            //sie ja nur eine Capacity von 1 hat. Sonst wartet die Queue bis sie wieder leer ist.
            queue.put(message);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }
}