package _22_23_WiSe._20221205_Executor_Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class _01_Executor_simple {

    public static void main(String[] args) {
        
        // Der Cached Thread Pool erstellt so viele Threads wie Runnables übergeben werden und
        // verwendet Threads von bereits erledigten Runnables wieder
        //ExecutorService exec = Executors.newCachedThreadPool();
        // Fixed Thread Pool erstellt nur bis zu nThreads und arbeitet in diesen die übergebenen
        // Runnables ab
        ExecutorService exec = Executors.newFixedThreadPool(3);       

        int counter = 0;

        while(true){

            System.out.printf("%d Threads laufen gerade %n", Thread.activeCount());

            if(counter < 10){
                Job run = new Job(5, "Job-"+counter+1);
                exec.execute(run);
            } 

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            counter++;
        }
        
    }   

    
}

class Job implements Runnable {
    int runs;
    String name;

    public Job(int runs, String name){
        this.runs = runs;
        this.name = name;
    }

    @Override
    public void run(){
        System.out.printf("* Starte Thread %s %n", name);
        for(int i = 0; i < runs; i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.printf("STOPPE Thread %s %n", name);

    }


}