package _22_23_WiSe._20221205_Generics;

public class _01_my_linked_list {

    public static void main(String[] args) {

        System.out.println("Starte belegen der Liste");
        MyLinkedListV1<String> myList = new MyLinkedListV1<>();
        myList.add("String 1");
        myList.add("String 2");
        myList.add("String 3");
        System.out.println("Printe Liste");
        System.out.println(myList);

        MyLinkedListV1<Integer> integerList = new MyLinkedListV1<>();

        integerList.add(10);
        // Würde Fehler erzeugen, weil Generic auf Integer gesetzt
        // integerList.add("String sowieso");
        
    }
    
}

class MyLinkedListV1<M> {

    public ElementContainer<M> head;

    public void add(M string) {
        ElementContainer<M> newElement = new ElementContainer<>(string);
        if(head == null){
            head = newElement;
        }
        else {
            ElementContainer<M> lastElement = this.lastElement();
            lastElement.nextElement = newElement;
        }

    }

    private ElementContainer<M> lastElement() {
        ElementContainer<M> lastElement = head;

        if(lastElement != null){
            while(lastElement.nextElement != null){
                lastElement = lastElement.nextElement;
            }
        }

        return lastElement;
    }

    @Override
    public String toString(){
        String returnString = "";

        ElementContainer<M> currentElement = head;

        int counter = 0;
        while(currentElement != null){
            returnString += String.format("Element %d: %s %n", counter, currentElement.data);
            currentElement = currentElement.nextElement;
            counter++;
        }

        return returnString;
    }

}

class ElementContainer<D> {

    public D data;
    public ElementContainer<D> nextElement;

    public ElementContainer(D data){
        this.data = data;
    }

}