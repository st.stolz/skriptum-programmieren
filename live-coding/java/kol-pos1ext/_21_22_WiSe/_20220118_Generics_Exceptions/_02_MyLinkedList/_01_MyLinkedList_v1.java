package _21_22_WiSe._20220118_Generics_Exceptions._02_MyLinkedList;

public class _01_MyLinkedList_v1 {

    public static void main(String[] args) {
        MyLinkedListV1<String> myList = new MyLinkedListV1<>();
        myList.add("Erstes Element");
        myList.add("Zweites Element");
        myList.add("Drittes Element");
        System.out.println(myList);
    }
    
}

class MyLinkedListV1<T>{

    public Element<T> head;

    public void add(T data){

        Element<T> newElement = new Element<>(data);

        if(head == null){
            head = newElement;
        }
        else {
            Element<T> lastElement = lastElement();
            lastElement.nextElement = newElement;
        }

    }

    private Element<T> lastElement() {
        Element<T> lastElement = head;

        while(lastElement.nextElement != null){
            lastElement = lastElement.nextElement;
        }

        return lastElement;
    }

    @Override
    public String toString(){
        String returnString = "";

        Element<T> currentElement = head;

        while(currentElement != null){
            returnString += currentElement.data;
            returnString += "\n";
            currentElement = currentElement.nextElement;
        }

        return returnString;
    }

}

class Element<D> {
    public D data;
    public Element<D> nextElement;

    public Element(D data){
        this.data = data;
    }
}