package _21_22_WiSe._20220118_Generics_Exceptions._01_Executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class _01_Executor_simple {
    public static void main(String[] args) {
        //ExecutorService exec = Executors.newFixedThreadPool(5);
        ExecutorService exec = Executors.newCachedThreadPool();

        exec.execute(new Job(10));

        Job run2 = new Job(10);

        int counter = 0;
        while (true) {
            if(counter < 10) exec.execute(run2);
            System.out.printf("%d Threads laufen gerade%n", Thread.activeCount());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
        }
    }
}

class Job implements Runnable {

    int runs;

    public Job(int runs) {
        this.runs = runs;
    }

    @Override
    public void run() {

        for (int i = 0; i < runs; i++) {
            // System.out.printf("%s: %d %n", Thread.currentThread().getName(), i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
