package _21_22_WiSe._20211012_Runnable;

public class _01_Runnable {
    public static void main(String[] args) {
        Thread t1 = new Thread(new Job(20), "Job 1");
        // Würde nur die Run Methode ohne Nebenläufigkeit ausführen: t1.run();
        t1.start();
        Thread t2 = new Thread(new Job(20), "Job 2");
        t2.start();
    }
}

class Job implements Runnable{

    int runs;

    public Job(int runs){
        this.runs = runs;
    }

    @Override
    public void run() {
        
        for (int i = 0; i < runs; i++) {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName+": "+i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
    }

}

class DataStorage {
    static int counter;
}
