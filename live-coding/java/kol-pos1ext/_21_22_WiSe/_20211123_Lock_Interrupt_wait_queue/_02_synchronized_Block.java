package _21_22_WiSe._20211123_Lock_Interrupt_wait_queue;

import java.util.LinkedList;

/**
 * Die folgende Klasse verwendet die Block Variante von
 * synchronized. Der Vorteil ist, dass man so nur einen
 * Teil einer Methode synchronisieren kann. Weiteres ist 
 * es möglich ein Objekt für die Synchronisation anzugeben,
 * was synchronized flexibler macht. 
 */
public class _02_synchronized_Block {
    
    public static void main(String[] args) {

        LinkedList<Thread> tList = new LinkedList<>();

        final int THREADS_N = 10;

        Object syncObject = new Object();
        
        for (int i = 0; i < THREADS_N; i++) {
            Runnable runner = new Runner3(syncObject);
            Thread t = new Thread(runner);
            t.start();
            tList.add(t);
        }

        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }    

        System.out.printf("Endstand Counter: %d %n", Storage.counter);

    }
}

class Runner3 implements Runnable {

    final int COUNT_TIMES = 1000;

    Object syncObject;

    public Runner3(Object syncObject){
        this.syncObject = syncObject;
    }

    @Override
    public void run() {
        for (int i = 0; i < COUNT_TIMES; i++) {
            synchronized(syncObject){
                Storage.counter ++;          
            }        
        }     
    }
    
}