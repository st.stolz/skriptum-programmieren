package _21_22_WiSe._20211123_Lock_Interrupt_wait_queue;

/**
 * wait() und notify() sind low level Methoden, die in der
 * Object Klasse definiert sind und somit jedem Java Objekt
 * zu Verfügung stehen. Low level Methoden sind aber meist
 * auch recht kompliziert zu verwenden. Für 2 Punkte Kandidaten
 * sollte es zumindest möglich sein, den folgenden Code
 * erklären zu können.
 * 
 * Es werden im Folgenden 2 Threads gestartet. Ein Sender und 
 * ein Empfänger. Es geht nun darum das Problem zu lösen, dass
 * der Empfänger so lange warten soll, bis neue Daten durch
 * den Sender übermittelt wurden. 
 * 
 * Wenn dasselbe Objekt in zwei verschiedenen Threads verwendet 
 * wird, kann für diese Aufgabe wait() und notify() verwendet 
 * werden.
 */
public class _04_wait_notify_3Punkte_Kandidaten {    
    public static void main(String[] args) {

        DataShareWait ds = new DataShareWait();

        Runnable sender = () -> {
            for (int i = 0; i < 1000; i++) {
                ds.setDataString("Message: " + Integer.toString(i));
            }
        };

        Runnable receiver = () -> {
            for (int i = 0; i < 1000; i++) {                                
                System.out.println(ds.getDataString());
            }
        };

        new Thread(sender).start();
        new Thread(receiver).start();
        
    }
}

class DataShareWait {

    private String dataString;
    boolean isSet = false;

    synchronized public String getDataString(){
        // findest du heraus, weshalb diese Schleife wichtig ist?
        while(!isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }   
        String returnVal = dataString;
        
        isSet = false;
        notify();
        return returnVal;
    }

    synchronized public void setDataString(String dataString){
        while(isSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }    
        System.out.println("Set: "+dataString);    
        this.dataString = dataString;
        isSet = true;
        notify();
    }
}
