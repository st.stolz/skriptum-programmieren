package _21_22_WiSe._20211123_Lock_Interrupt_wait_queue;

import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * In der folgenden Klasse sieht man, wie anstatt von 
 * synchronized() ein Lock Objekt verwendet wird. Diese
 * Variante ist flexibler, jedoch ist es noch riskanter
 * Dead Locks zu übersehen. 
 * 
 * Der ReentrantLock ermöglicht es einem Thread, mehrfach
 * einen Lock zu betreten.
 */
public class _03_Lock {
    public static void main(String[] args) {

        LinkedList<Thread> tList = new LinkedList<>();

        final int THREADS_N = 10;

        ReentrantLock lock = new ReentrantLock();
        
        for (int i = 0; i < THREADS_N; i++) {
            Runnable runner = new Runner2(lock);
            Thread t = new Thread(runner);
            t.start();
            tList.add(t);
        }

        for (Thread thread : tList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }    

        System.out.printf("Endstand Counter: %d %n", Storage.counter);

    }
}

class Runner2 implements Runnable {

    final int COUNT_TIMES = 1000;

    ReentrantLock lock;

    public Runner2(ReentrantLock lock){
        this.lock = lock;
    }

    @Override
    public void run() {
        for (int i = 0; i < COUNT_TIMES; i++) {
            lock.lock();
                Storage.counter ++;          
            lock.unlock();       
        }     
    }
    
}