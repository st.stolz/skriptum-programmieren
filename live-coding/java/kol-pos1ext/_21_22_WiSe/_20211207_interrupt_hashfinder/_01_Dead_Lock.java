package _21_22_WiSe._20211207_interrupt_hashfinder;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class _01_Dead_Lock {

    static Lock lock1 = new ReentrantLock();
	static Lock lock2 = new ReentrantLock();

	public static void main(String[] args) {
		
		Runnable run1 = () -> {
			while(true){
				lock1.lock();
				lock2.lock();

				System.out.printf("%s works!", Thread.currentThread().getName());
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				lock1.unlock();
				lock2.unlock();
				
			}
		};

		Runnable run2 = () -> {
			while(true){
				lock2.lock();
				lock1.lock();				

				System.out.printf("%s works!", Thread.currentThread().getName());
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
				lock1.unlock();
				lock2.unlock();
				
			}
		};

		new Thread(run1,"run1").start();
		new Thread(run2,"run2").start();

	}

}
