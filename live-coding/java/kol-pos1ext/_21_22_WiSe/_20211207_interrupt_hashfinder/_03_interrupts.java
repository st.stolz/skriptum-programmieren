package _21_22_WiSe._20211207_interrupt_hashfinder;

public class _03_interrupts {
    public static void main(String[] args) {
        Runnable runner = () -> {
            while(!Thread.currentThread().isInterrupted()){
                System.out.println("I am running!");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
            }
        };

        Thread t1 = new Thread(runner);
        t1.start();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t1.interrupt();
    }
}
