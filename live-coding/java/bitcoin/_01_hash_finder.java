import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class _01_hash_finder {
    public static void main(String[] args) {
        String blockData = "Block Header";
        // The difficulty is adjusted every 2016 blocks based on the time it took to find the previous 2016 blocks. 
        // At the desired rate of one block each 10 minutes, 2016 blocks would take exactly two weeks to find. 
        // If the previous 2016 blocks took more than two weeks to find, the difficulty is reduced. 
        // If they took less than two weeks, the difficulty is increased. 
        // The change in difficulty is in proportion to the amount of time over or under two weeks the previous 2016 blocks took to find.
        HashFinder.findHash(blockData, "00");
    }
}

class HashFinder {

    public static boolean findHash(String blockData, String difficulty){
        boolean isFound = false;
        long nonce = 0;

        while(!isFound && nonce < Long.MAX_VALUE){
            String strToHash = blockData+nonce;
            String hash = sha256(strToHash);
            System.out.printf("\r %4d - %s - %s ",nonce,Thread.currentThread().getName(),hash);

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(hash.startsWith(difficulty)){
                isFound = true;
                System.out.printf("%n Gesuchter Hash mit %d Versuchen gefunden %n",nonce+1);
            }
            nonce++;
        }

        return isFound;
    }

    /**
     * Calculates sha256 to a String
     * 
     * @param strToHash
     * @return
     */
    public static String sha256(String strToHash) {

        byte[] bytesOfMessage;
        try {
            bytesOfMessage = strToHash.getBytes("UTF-8");        
            MessageDigest md;        
            md = MessageDigest.getInstance("SHA-256");        
        
            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                String hex = Integer.toHexString(0xff & thedigest[i]);
                if(hex.length() == 1) 
                        hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } 
        catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        
    }
}
