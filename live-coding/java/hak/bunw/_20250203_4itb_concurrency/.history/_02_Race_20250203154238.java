public class _02_Race {
    public static void main(String[] args) {
        Runnable run = new Job2();
        for (int i = 0; i < 10; i++) {
            Thread t = new Thread(run);
            t.start();
        }
        System.out.printf("Endstand Counter: %d %n",Storage.counter);
    }
}


class Job2 implements Runnable {    

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            Storage.counter = Storage.counter + 1;
        }        
    }
    
}

class Storage {
    static int counter = 0;
}
