public class _01_Runnable {
    public static void main(String[] args) {
        System.out.printf("Starting Thread Name: %s %n", Thread.currentThread().getName());

        Job job = new Job(20);
        Thread thread1 = new Thread(job,"Thread1");

        // run() führt einfach die run() Methode des Runnable aus ohne einen Thread zu starten
        // thread1.run();

        thread1.start();
    }
}

class Job implements Runnable {

    int runs;

    public Job(int runs){
        this.runs = runs;
    }

    @Override
    public void run() {
        for (int i = 0; i < runs; i++) {
            System.out.printf("%03d von %s %n", i, Thread.currentThread().getName());

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}