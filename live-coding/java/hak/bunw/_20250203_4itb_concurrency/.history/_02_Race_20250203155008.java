import java.util.ArrayList;

public class _02_Race {
    public static void main(String[] args) {
        Runnable run = new Job2();
        ArrayList<Thread> threads = new ArrayList<Thread>();
        for (int i = 0; i < 10; i++) {
            Thread t = new Thread(run);
            threads.add(t);
            t.start();
        }
        for(Thread t : threads){
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.printf("Endstand Counter: %d %n",Storage.counter);
    }
}


class Job2 implements Runnable {    

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            Storage.counter = Storage.counter + 1;
        }        
    }
    
}

class Storage {
    static int counter = 0;
}
