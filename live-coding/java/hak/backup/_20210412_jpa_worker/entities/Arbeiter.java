package _20210412_jpa_worker.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Arbeiter extends Angestellter {

    @Id @GeneratedValue
    private int id;

    int zuschuss;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getZuschuss() {
        return zuschuss;
    }

    public void setZuschuss(int zuschuss) {
        this.zuschuss = zuschuss;
    }

}
