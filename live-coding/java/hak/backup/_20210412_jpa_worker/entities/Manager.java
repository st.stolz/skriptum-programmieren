package _20210412_jpa_worker.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Manager extends Angestellter {

    @Id @GeneratedValue
    private int id;

    private int beteiligung;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBeteiligung() {
        return beteiligung;
    }

    public void setBeteiligung(int beteiligung) {
        this.beteiligung = beteiligung;
    }
    
}
