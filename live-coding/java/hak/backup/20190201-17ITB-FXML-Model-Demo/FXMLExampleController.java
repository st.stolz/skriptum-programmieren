import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;
 
public class FXMLExampleController {
    @FXML private Text actiontarget;
    
    @FXML private TextField userName;
    
    @FXML protected void handleSubmitButtonAction(ActionEvent event) {
        actiontarget.setText(userName.getCharacters().toString());
        UserDatabaseModel.addUser(userName.getCharacters().toString(), userName.getCharacters().toString());
    }

}