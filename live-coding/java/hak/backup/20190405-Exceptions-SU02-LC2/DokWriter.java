import java.nio.file.Files;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.FileAlreadyExistsException;

public class DokWriter
{
    
    public static void newVersion(String data){
        FileWorker.addNewVersion(data);
    }
   
}

class FileWorker
{
    public final static String FILE_NAME = "filename";
    
    public static void addNewVersion(String data){
        boolean running = true;
        int counter = 0;
        String filename = ""; 
        while(running && counter < 100){
            counter++;
            filename = String.format("%s-%03d",FILE_NAME,counter);
            try {
                Files.createFile(Paths.get(filename));
                running = false;
            }
            catch(FileAlreadyExistsException seppi){                
                running = true;
            }   
            catch(IOException e){
                System.err.printf("Kann Datei %s nicht erstellen: %s%n",FILE_NAME,e.toString());
                System.exit(0);
            }
        }
        PrintWriter out = null;
        try {
            out = new PrintWriter(filename);
            out.println(data);
        }
        catch(IOException e){
            System.err.printf("Kann in Datei %s nicht schreiben: %s%n",FILE_NAME,e);
        }
        finally{
            out.close();
        }
    }
}