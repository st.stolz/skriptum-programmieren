package _20200615_Vererbung_Basics;
public class Medium
{
    private int price;
    private String titel;
    
    public Medium()
    {
       
    }
    
    public void setTitel(String titel){
        this.titel = titel;
    }
    
    public String getTitel(){
        return titel;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    
}
