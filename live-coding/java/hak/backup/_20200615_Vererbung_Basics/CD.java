package _20200615_Vererbung_Basics;

public class CD extends Medium
{
   
    private String interpret;
    private int durationMinutes;
    
    public CD()
    {
        
    }
    
    public void setTitel(String titel){
        super.setTitel(titel);
    }   
    
    public String getTitel(){
        return super.getTitel();
    }

    public String getInterpret() {
        return interpret;
    }

    public void setInterpret(String interpret) {
        this.interpret = interpret;
    }

    public int getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(int durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    
}
