package _20200615_Vererbung_Basics;

public class Book extends Medium
{
    
    private int pageN;
    private String author;    
    
    public Book()
    {
        
    }
    
    public void setPageN(int pageN){
        this.pageN = pageN;
    }
    
    public int getPageN(){
        return pageN;
    }
    
    public void setAuthor(String author){
        this.author = author;
    }
    
    public String getAuthor(){
        return author;
    }
    
    public void setTitel(String titel){
        super.setTitel(titel);
    }
    
    public String getTitel(){
        return super.getTitel();
    }
}
