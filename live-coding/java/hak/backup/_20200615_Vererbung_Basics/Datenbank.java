package _20200615_Vererbung_Basics;
import java.util.ArrayList;

public class Datenbank
{
    
    ArrayList<Medium> database;
    
    public Datenbank()
    {
         database = new ArrayList<>();
    }
    
    public void initDatabase(){
        Book book1 = new Book();
        book1.setPageN(1600);
        book1.setAuthor("Tolkien");
        book1.setTitel("Herr der Ringe");
        database.add(book1);
        
        CD cd1 = new CD();
        cd1.setTitel("Imaginations from the other side");
        database.add(cd1);
    }
    
    public void printDatabase(){
        System.out.println(toString());
    }
    
    public String toString(){
        String returnStr = "";
        for(Medium m : database){
            returnStr = returnStr + m.getTitel() + "\n";
            if(m instanceof Book){
                //m.getAuthor(); // geht nicht
                Book book = (Book) m;
                returnStr += book.getAuthor() + "\n";
            }
        }
        return returnStr;
    }
}
