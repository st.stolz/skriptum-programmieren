
public class Fly
{
    Position pos;
    char image;
    
    public Fly(Position pos)
    {
        this.pos = pos;
        this.image = 'ö';        
    }
    
    Position getPos(){
        return this.pos;
    }
    
    char getImage(){
        return this.image;
    }
    
    void setPos(Position pos){
        this.pos = pos;
    }
    
    void setImage(char image){
        this.image = image;
    }
}
