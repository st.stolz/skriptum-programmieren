import java.util.ArrayList;

public class Application
{
    ArrayList<Spider> spiderList;
    ArrayList<Fly> flyList;
    
    int size_x = 4;
    int size_y = 4;
    
    public Application()
    {
        spiderList = new ArrayList<>();
        flyList = new ArrayList<>();
        Position pos = new Position(0,1);
        Spider spider = new Spider(pos);
        pos = new Position(0,2);
        Fly fly = new Fly(pos);
        
        spiderList.add(spider);
        flyList.add(fly);
    }

    void drawWorld(){
        for(int y = 0; y < size_y; y++){
            for(int x = 0; x < size_x; x++){
                Position pos = new Position(x,y);
                Character image = checkIfArthOnPos(pos);
                if(image == null){
                    System.out.print("x");
                }
                else{
                    System.out.print(image);
                }
            }
            System.out.println();
        }
    }
    
    Character checkIfArthOnPos(Position pos){
        Character returnVal = null;
        
        for(Spider spider : spiderList){
            if(spider.getPos().getX() == pos.getX() && spider.getPos().getY() == pos.getY()){
                returnVal = spider.getImage();
            }
        }
        
        for(Fly fly : flyList){
            if(fly.getPos().getX() == pos.getX() && fly.getPos().getY() == pos.getY()){
                returnVal = fly.getImage();
            }
        }
        
        return returnVal;
    }
}
