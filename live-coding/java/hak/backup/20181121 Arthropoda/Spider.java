public class Spider
{
    Position pos;
    char image;
    
    public Spider(Position pos)
    {
        this.pos = pos;
        this.image = 'x';        
    }
    
    public Spider(Position pos, char image)
    {
        this.pos = pos;
        this.image = image;        
    }
    
    Position getPos(){
        return this.pos;
    }
    
    char getImage(){
        return this.image;
    }
    
    void setPos(Position pos){
        this.pos = pos;
    }
    
    void setImage(char image){
        this.image = image;
    }
}