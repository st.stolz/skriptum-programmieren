# Handmade Histogramm

BlueJ Code, um mit JavaFX ein simples Histogramm zu erstellen. Die verwendeten Bilder sollten nicht mehr als 100 x 100 Pixel haben.

## Beispiel

![Histogramm hell dunkel](images/bright-dark-histo.png)