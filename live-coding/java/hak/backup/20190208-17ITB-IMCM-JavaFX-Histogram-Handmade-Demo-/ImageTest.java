import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.image.Raster;

import javax.imageio.ImageIO;

import java.util.ArrayList;

public class ImageTest
{      
    
    public ArrayList<Integer> calcHistogram() throws IOException
    {
        ArrayList<Integer> histoArr = new ArrayList<>();
        
        for(int i = 0; i < 256; i++){
            histoArr.add(0);
        }
        
        File file = new File("wald-hell.jpg");
        BufferedImage image = ImageIO.read(file);
        
        Raster raster = image.getData();
        
        for(int x = 0; x < image.getWidth(); x++){
            for(int y = 0; y < image.getHeight(); y++){
                int value = raster.getSample(x,y,0);
                //System.out.println(String.format("(x: %d, y: %d) %d",x,y,value));
                
                histoArr.set(value,histoArr.get(value)+1);
            }
        }
        return histoArr;
    }
}