import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.io.IOException;

public class FXHistoGram extends Application {
    
    final int vSize = 250;
    final int hSize = 255;

    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("Drawing Operations Test");
        Group root = new Group();
        Canvas canvas = new Canvas(hSize, vSize);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        drawHistogram(gc);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
    
    private void drawHistogram(GraphicsContext gc) throws IOException {
        
        ImageTest it = new ImageTest();
        ArrayList<Integer> histoArr = it.calcHistogram();
        
        int maxVal = 0;
        for(Integer val : histoArr){
            if(val>maxVal){
                maxVal = val;
            }
        }
        
        int sizeFac = 255 / maxVal;
        
        for(int i = 0; i < histoArr.size(); i++){
            gc.setStroke(Color.BLUE);
            gc.setLineWidth(5);
            gc.strokeLine(i, 256, i, 256-(histoArr.get(i)*sizeFac));      
        }
          
    }
}
