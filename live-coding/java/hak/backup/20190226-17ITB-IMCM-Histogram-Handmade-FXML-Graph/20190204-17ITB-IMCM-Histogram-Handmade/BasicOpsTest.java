import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;

import java.io.IOException;

import java.util.ArrayList;
 
public class BasicOpsTest extends Application {
 
    final int CANVAS_V = 250;
    final int CANVAS_H = 300;
    
    public static void main(String[] args) {
        launch(args);
    }
 
    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("Drawing Operations Test");
        Group root = new Group();
        Canvas canvas = new Canvas(CANVAS_H, CANVAS_V);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        drawShapes(gc);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    private void drawShapes(GraphicsContext gc) throws IOException {     
        
        ArrayList<Integer> histo = HistoArray.calcHistoArray();
        
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(1);        
        
        Integer max_val = 0;
        for(int i = 0; i < 256; i++){
            Integer value = histo.get(i);
            if(value > max_val){
                max_val = value;
            }
        }
        
        Integer rel = CANVAS_V / max_val;
        
        for(int i = 0; i < 256; i++){
            Integer value = histo.get(i);
            gc.strokeLine(i, CANVAS_V, i, CANVAS_V-(value*rel));
        }
    }
}