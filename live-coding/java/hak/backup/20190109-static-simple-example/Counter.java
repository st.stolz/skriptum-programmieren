public class Counter
{
    private static int countN;
    private static final int countMax = 10;
    
    public Counter()
    {
        this.countN = 0;
    }

    public static boolean count(){
        boolean returnIfSuccess = false;
        if(countN < countMax){
            countN = countN + 1;
            returnIfSuccess = true;
        }
        return returnIfSuccess;
    }
    
    public static int getCounter(){
        return countN;
    }
}
