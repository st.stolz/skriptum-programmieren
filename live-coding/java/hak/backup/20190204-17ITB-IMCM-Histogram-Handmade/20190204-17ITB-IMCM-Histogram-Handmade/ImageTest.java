import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.image.Raster;

import javax.imageio.ImageIO;

import java.util.ArrayList;

public class ImageTest
{      
    
    public static void main(final String args[]) throws IOException
    {
        File file = new File("gray_dots.png");
        BufferedImage image = ImageIO.read(file);
        
        for(int x = 0; x < image.getWidth(); x++){
            for(int y = 0; y < image.getHeight(); y++){
                int value = image.getRGB(x,y) & 0xFF;
                System.out.println(String.format("(x: %d, y: %d) %d",x,y,value));
            }
        }
        
        System.out.println("-----------");
        
        Raster raster = image.getData();
        
        for(int x = 0; x < image.getWidth(); x++){
            for(int y = 0; y < image.getHeight(); y++){
                int value = raster.getSample(x,y,0);
                System.out.println(String.format("(x: %d, y: %d) %d",x,y,value));
            }
        }
        
        
    }
}