import java.util.Scanner;

public class Selector
{
    private static String[] manufacturers = {"Volvo", "BMW", "Ford", "Mazda"};
    private static Scanner scanner = new Scanner(System.in);

    public static void main(){
        
        System.out.printf("Select your favorite manufacturer:%n%n");
        
        for(int i = 0; i < manufacturers.length; i++){
            System.out.printf("%d) %s%n",i,manufacturers[i]);
        }
        int counter = 0;
        boolean running = true;
        while(running && counter < 10){
            counter++;
            System.out.printf("%nType in manufacturer number:");
            int number = scanner.nextInt();
            
            try{
                System.out.printf("%nYou selected the manufacturer: %s",manufacturers[number]);
                running = false;
            }
            catch(ArrayIndexOutOfBoundsException e){
                System.out.printf("%nEnter a valid number");
            }
            
        }
        
        if(counter > 9){
                System.out.printf("%nTo many bad tries. Please restart.");
        }
            
    }
}
