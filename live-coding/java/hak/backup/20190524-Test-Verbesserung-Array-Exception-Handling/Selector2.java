import java.util.Scanner;

public class Selector2
{
    private static String[] manufacturers = {"Volvo", "BMW", "Ford", "Mazda"};
    private static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args){
        int counter = 0;
   boolean running = true;
        
        while(counter < 10 && running){
            counter++;
            try{           
          System.out.printf("Select your favorite manufacturer:%n%n");
        
        for(int i = 0; i < manufacturers.length; i++){
            System.out.printf("%d) %s%n",i,manufacturers[i]);
        }
        
        System.out.printf("%nType in manufacturer number:");
        int number = scanner.nextInt();
        running = false;
        System.out.printf("%nYou selected the manufacturer: %s",manufacturers[number]);
    }
            catch(IndexOutOfBoundsException se){
            running = true;
            se.printStackTrace();
            System.out.println("Keine Datei gefunden");
        }
    
   }
    }
   
    }