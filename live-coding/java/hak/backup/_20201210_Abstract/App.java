package _20201210_Abstract;

public class App {
    public static void main(String[] args) {

        Beetle beetle = new Beetle(9);

        Fighter fighter1 = beetle;
        fighter1.fight();

        Insect[] insects = new Insect[10];
        insects[0] = new Beetle(10);
        System.out.println(insects[0].getXPosition());
        insects[0].move(3);
        // insects[0].fight();
        insects[0].eat();
        System.out.println(insects[0].getXPosition());
    }
}

abstract class Insect {

    private int xPosition;

    public Insect(int xPosition){
        this.xPosition = xPosition;
    }

    public void move(int distance){
        xPosition += distance;
    }

    public int getXPosition(){
        return xPosition;
    }
    

    public abstract void eat();
}

abstract class Mammalian {
    private int xPosition;

    public Mammalian(int xPosition){
        this.xPosition = xPosition;
    }

    public void move(int distance){
        xPosition += distance;
        xPosition += distance;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    
}

interface Fighter extends Runner{
    public void fight();
}

interface Runner {
    public void run();
}

class Beetle extends Insect implements Fighter {

    public Beetle(int xPosition) {
        super(xPosition);
    }

    @Override
    public void fight() {
        System.out.println("Beetle attacks with his poison cloud");
    }

    @Override
    public void run() {
        System.out.println("Beetle runs in panic");
    }
    
    @Override
    public void eat(){
        System.out.println("Beetle eats");
    }
    
}