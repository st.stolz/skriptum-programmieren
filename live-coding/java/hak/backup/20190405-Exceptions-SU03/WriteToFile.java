import java.util.Scanner;

public class WriteToFile
{
    public static void testString(String testString){
        try{
            FileNameParser.testIt(testString);
        }
        catch(NoValidFilenameException e){
            e.printStackTrace();
        }
    }
}

class FileNameParser {
    public static void testIt(String testString) throws NoValidFilenameException
    {
        
        String myString = testString;
        Scanner scanner = new Scanner(myString);
        scanner.useDelimiter("-");
        while(scanner.hasNext()){
            System.out.println(scanner.next());
        }
        throw new NoValidFilenameException(myString);
    }
}

class NoValidFilenameException extends Exception {
    String filename;
    public NoValidFilenameException(String filename){
        this.filename = filename;
    }
    public String toString(){
        return String.format("Der Dateiname %s entspricht nicht dem vorgegebenen Format",filename);
    }
}
