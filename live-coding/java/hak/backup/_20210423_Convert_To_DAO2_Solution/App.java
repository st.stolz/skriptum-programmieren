package _20210423_Convert_To_DAO2_Solution;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class App {    

    
    public static void main(String[] args) {
        
        PersonDAO pDao = new PersonDAO();
        EmailDAO eDao = new EmailDAO();

        pDao.save(new Person("Stolz"));
        eDao.save(new Email("s.stolz@tsn.at", 1));
        eDao.save(new Email("s.newslists@gmail.com", 1));
    }

}

class PersonDAO{

    static String url = "jdbc:sqlite:./src/_20210423_Convert_To_DAO2_Solution/mydb.db";

    public PersonDAO(){
        createNewTable();
    }

    public void save(Person person){
        String name = person.getName();
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String sql = "INSERT INTO persons(name) VALUES(?)";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void createNewTable() {

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String sql_persons = "CREATE TABLE IF NOT EXISTS persons (\n"
                + "	id integer not null PRIMARY KEY,\n"
                + "	name varchar(255)\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql_persons);
            System.out.println("Table persons is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

class EmailDAO{

    static String url = "jdbc:sqlite:mydb.db";

    public EmailDAO(){
        createNewTable();
    }

    public void save(Email email){
        String address = email.getAddress();
        int person_id = email.getPerson_id();
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String sql = "INSERT INTO emails(address, person_id) VALUES(?, ?)";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, address);
            pstmt.setInt(2, person_id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void createNewTable() {

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String sql_emails = "CREATE TABLE IF NOT EXISTS emails (\n"
                + "	id integer not null PRIMARY KEY,\n"
                + "	address varchar(255),\n"
                + "	person_id int\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql_emails);
            System.out.println("Table emails is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

class Person{

    private String name;

    public Person(String name){
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}

class Email{

    private String address;
    private int person_id;

    public Email(String address, int person_id){
        this.address = address;
        this.person_id = person_id;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }

    public int getPerson_id() {
        return person_id;
    }

}
