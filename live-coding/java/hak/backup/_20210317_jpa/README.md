## JPA Libs

Aus folgenden Quellen die jar Dateien downloaden und **ohne entpacken** dem Projekt hinzufügen.

* https://mvnrepository.com/artifact/mysql/mysql-connector-java/8.0.23
* https://mvnrepository.com/artifact/org.eclipse.persistence/eclipselink/3.0.0
* https://mvnrepository.com/artifact/jakarta.persistence/jakarta.persistence-api/3.0.0
* https://mvnrepository.com/artifact/com.h2database/h2/1.4.200

## Gute JPA Tutorials

Achtung die meisten Tutorials verwenden noch das javax.* Package. Aktuell ist aber stattdessen jakarta.* zu verwenden. 

* https://www.tutorialspoint.com/jpa/index.htm
* https://www.objectdb.com/java/jpa

## Eclipselink Specials

* https://wiki.eclipse.org/EclipseLink/Examples/JPA/Inheritance
* https://vladmihalcea.com/the-best-way-to-map-a-onetomany-association-with-jpa-and-hibernate/


