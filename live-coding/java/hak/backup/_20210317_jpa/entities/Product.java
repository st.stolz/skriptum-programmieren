package _20210317_jpa.entities;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Product {

    @Id @GeneratedValue
    private int id;

    String name;

    @OneToMany( targetEntity=OrderPositions.class )
    private List<OrderPositions> positionlist;
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OrderPositions> getPositionlist() {
        return positionlist;
    }

    public void setPositionlist(List<OrderPositions> positionlist) {
        this.positionlist = positionlist;
    }

    @Override
    public String toString() {
        return "Product [id=" + id + ", name=" + name + ", positionlist=" + positionlist + "]";
    }

    
    
    
    
    
}
