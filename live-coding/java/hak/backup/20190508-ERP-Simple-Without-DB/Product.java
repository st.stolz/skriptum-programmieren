

public class Product
{
    private String name;
    private int price;

    public Product(String name, int price)
    {
        this.name = name;
        this.price = price;
    }

    /** 
     *  1. Nenne typische Bestandteile einer Klasse
     * 
     *  2. Ist es in dieser Klasse möglich, 
     *     aus einer anderen Klasse (Objekt) auf alle
     *     Datenfelder zuzugreifen? Wenn nein: Was kann
     *     getan werden, dass es möglich ist?
     * 
     *  --> remove
     */
    public String getName(){
        return this.name;
    }
    
    public int getPrice(){
        return this.price;
    }
    /**
     * <-- remove
     */
}
