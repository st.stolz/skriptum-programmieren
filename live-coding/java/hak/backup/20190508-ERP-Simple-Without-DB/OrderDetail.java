
public class OrderDetail
{
    private int amount;
    private Product product;

    public OrderDetail(int amount,Product product)
    {
        this.amount = amount;
        this.product = product;
    }

    public int getAmount(){
        return this.amount;
    }
    
    public Product getProduct(){
        return this.product;
    }
}
