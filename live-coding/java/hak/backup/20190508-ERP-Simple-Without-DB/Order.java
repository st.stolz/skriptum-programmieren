import java.util.ArrayList;
import java.util.Iterator;

public class Order
{
    private ArrayList<OrderDetail> orderDetail;
    
    public Order()
    {
        this.orderDetail = new ArrayList<>();
    }

    public void addOrderDetail(OrderDetail orderDetail){
        this.orderDetail.add(orderDetail);
    }
    
    /**
     * 1. Erstelle eine Klasse showOrders(), die öffentlich zugänglich ist und 
     *    die alle gespeicherten Produkte in folgender Form ausgibt: 
     *    "Menge - Produktname - Preis - Gesamtpreis" 
     *    Zeige dies anhand der iterator Klasse.
     * 
     * 2. Wann sollte die Iterator Klasse verwendet werden? Wann ist die foreach
     *    Schleife vorzuziehen? Was würdest du in diesem Fall wählen, könntest
     *    du frei entscheiden?
     * 
     * 3. Wo beim Iterator / Liste findet man Generics? Welche Vorteile bietet
     *    es, diese zu verwenden?
     * 
     *  --> remove
     */
    public void showOrders(){
        Iterator<OrderDetail> it = this.orderDetail.iterator();
        while(it.hasNext()){
            OrderDetail od = it.next();
            String outputStr = "";
            outputStr += od.getAmount();
            outputStr += " - ";
            outputStr += od.getProduct().getName();
            outputStr += " - ";
            outputStr += od.getProduct().getPrice();            
            System.out.println(outputStr);
            
        }
    }
    /**
     * <-- remove
     */
}
