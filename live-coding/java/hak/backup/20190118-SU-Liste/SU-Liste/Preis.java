
public class Preis
{
    private int preis;
    private String datum;

    public Preis(int preis, String datum)
    {
        this.preis = preis;
        this.datum = datum;
    }

    public int getPreis(){
        return preis;
    }
    
    public String datum(){
        return datum;
    }
}
