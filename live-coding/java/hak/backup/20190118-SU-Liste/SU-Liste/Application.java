import java.util.ArrayList;

public class Application
{
    private ArrayList<Preis> preisListe;

    public Application()
    {
        preisListe = new ArrayList<>();
    }
    
    public void addPreis(Preis preis){
        preisListe.add(preis);
    }

    public void durchschnittBerechnung(){
        int preisSumme = 0;
        for(Preis preis : preisListe){
            preisSumme = preisSumme + preis.getPreis();
        }
        int durchschnitt = preisSumme / preisListe.size();
        System.out.println(durchschnitt);
    }
}
