package _20210416_jpa_basics.entities; // Referenced in persistence.xml

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class AnotherThing {

  @Id @GeneratedValue
  private int id;

  private String text;

  public AnotherThing() {
  }

  public int getId() {
      return id;
  }

  public void setId(int id) {
      this.id = id;
  }

  public String getText() {
      return text;
  }
  
  public void setText(String text) {
      this.text = text;
  }
}