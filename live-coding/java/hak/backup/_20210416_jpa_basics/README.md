## Begrifflichkeiten

* **JPA** ist ein Standard / Spezifikation von Oracle, der das Java **ORM** definiert. Eclipselink ist derzeit die Refenzimplementierung.
* POJO - Plane Old Java Object

## JPA Libs

Aus folgenden Quellen die jar Dateien downloaden und **ohne entpacken** dem Projekt hinzufügen.

* https://mvnrepository.com/artifact/mysql/mysql-connector-java/8.0.23
* https://mvnrepository.com/artifact/org.eclipse.persistence/eclipselink/3.0.0
* https://mvnrepository.com/artifact/jakarta.persistence/jakarta.persistence-api/3.0.0
* https://mvnrepository.com/artifact/com.h2database/h2/1.4.200

## Gute JPA Tutorials

Achtung die meisten Tutorials verwenden noch das javax.* Package. Aktuell ist aber stattdessen jakarta.* zu verwenden. 

* https://www.tutorialspoint.com/jpa/index.htm
* https://www.objectdb.com/java/jpa

Ein gutes 1:n Beispiel: 

* https://www.vogella.com/tutorials/JavaPersistenceAPI/article.html#example

## Eclipselink Specials

* [Eclipselink Examples](https://wiki.eclipse.org/EclipseLink/Examples/JPA)
  * [Inheritance](https://wiki.eclipse.org/EclipseLink/Examples/JPA/Inheritance)
  * [Primary Key Generation](https://wiki.eclipse.org/EclipseLink/Examples/JPA/PrimaryKey)
  * [One-to-Many Mapping](https://wiki.eclipse.org/EclipseLink/UserGuide/JPA/Basic_JPA_Development/Mapping/Relationship_Mappings/Collection_Mappings/OneToMany)
  * Many-to-Many Mapping
  * One-to-One Mapping
  * Datentypen
  * Cashing ....


