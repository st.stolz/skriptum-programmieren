package _20210416_jpa_basics;

import java.util.List;

import _20210416_jpa_basics.entities.AnotherThing;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;

public class JPA_App {
    public static void main(String[] args) {
        // Setup the entity manager
        EntityManagerFactory factory =   Persistence.createEntityManagerFactory("h2Unit");
        EntityManager em = factory.createEntityManager();

        // Create it
        AnotherThing tEntity = new AnotherThing();
        tEntity.setText("Hello World!");  

        // Add it
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(tEntity);
        trans.commit();

        // Fetch them
        TypedQuery<AnotherThing> q = em.createQuery("select thing from AnotherThing thing", AnotherThing.class);
        //em.find(arg0, arg1)
        List<AnotherThing> results = q.getResultList();
        for (AnotherThing thing : results) {
            System.out.println(thing.getId() + ": " + thing.getText());
        }
        
        // Close the entity manager
        em.close();
        factory.close();
    }
}
