package _20210416_jpa_basics;

import _20210416_jpa_basics.entities.AnotherThing;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class App_entry {
    public static void main(String[] args) {
        // Setup the entity manager
        EntityManagerFactory factory =   Persistence.createEntityManagerFactory("h2Unit");
        EntityManager em = factory.createEntityManager();
        
        // Create it
        AnotherThing t = new AnotherThing();
        t.setText("Seppi");    	
        
        // Add it
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        em.persist(t);
        trans.commit();
        
        em.close();
        factory.close();
    }
}
