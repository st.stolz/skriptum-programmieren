import java.sql.*;
import java.lang.LinkageError;
import java.lang.ExceptionInInitializerError;
import java.lang.ClassNotFoundException;

public class JDBCExample {
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   static final String DB_URL = "jdbc:mysql://localhost/";

   static final String USER = "root";
   static final String PASS = "123";
   
   public static void main(String[] args) {
   Connection conn = null;
   Statement stmt = null;
   
   boolean running = true;
   int counter = 0;
   while(running && counter < 10){      
       
       try{
          counter++;
          
          Class.forName("com.mysql.jdbc.Driver");
          
          conn = DriverManager.getConnection(DB_URL, USER, PASS);
          
          stmt = conn.createStatement();
          
          String sql = "CREATE DATABASE STUDENTS";
          stmt.executeUpdate(sql);
          System.out.println("Database created successfully...");
          
          running = false;
       }
       catch(SQLTimeoutException e){
           e.printStackTrace();
           running = false;
       }
       catch(SQLException se){
          //Handle errors for JDBC
          se.printStackTrace();
          running = true;
       }       
       catch(LinkageError e1){
           e1.printStackTrace();
           running = false;
       }
       catch(ClassNotFoundException e3){
           e3.printStackTrace();
           running = false;
       }
       finally{
          try{
             if(stmt!=null)
                stmt.close();
          }catch(SQLException se2){
          }
          try{
             if(conn!=null)
                conn.close();
          }catch(SQLException se){
             se.printStackTrace();
          }
       }
    }
   System.out.println("Goodbye!");
}
}