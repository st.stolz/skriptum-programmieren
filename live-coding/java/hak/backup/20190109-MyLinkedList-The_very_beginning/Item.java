
public class Item<I>
{
    // Die Daten für dieses Element
    private I data;
    // Link zum nächsten Element
    private Item nextItem;

    /**
     * Item Constructor
     *
     * @param data Die Daten für das Element
     */
    public Item(I data)
    {
        this.data = data;
        this.nextItem = null;
    }    
    
    public Item getNextItem(){
        return nextItem;        
    }
    
    public void setNextItem(Item item){
        this.nextItem = item;
    }
}
