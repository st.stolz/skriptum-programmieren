
public class MyLinkedList<I>
{
    Item firstItem;

    /**
     * MyLinkedList Constructor
     *
     * initialisiert die Liste
     */
    public MyLinkedList()
    {
        firstItem = null;        
    }

    /**
     * Methode add
     * 
     * fügt der Liste neue Elemente hinzu
     *
     * @param data Die Daten für das neue Element
     */
    public void add(I data){
        Item currentItem = firstItem;
        Item previousItem = null;
        while (currentItem != null){
            previousItem = currentItem;
            currentItem = currentItem.getNextItem();
        }
        currentItem = new Item<I>(data);
        if(previousItem == null){
            firstItem = currentItem;
        }
        else{
            previousItem.setNextItem(currentItem);
        }
    }
    
    /**
     * Methode getItem
     * 
     * gibt das Element auf der gewählten Position index zurück
     *
     * @param index Position des Elements
     * @return Das gefundene Item
     */
    public Item getItem(int index){
        Item returnItem = null;
        // finde das Item auf der Position index
        return returnItem;
    }
}
