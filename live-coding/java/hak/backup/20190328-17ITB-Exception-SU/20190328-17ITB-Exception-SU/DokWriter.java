import java.nio.file.Files;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.io.IOException;
import java.nio.file.Paths;

public class DokWriter
{
    
    public static void newVersion(String data){
        FileWorker.addNewVersion(data);
    }
   
}

class FileWorker
{
    private static String FILE_NAME = "filename";
    public static void addNewVersion(String data) {
        int counter = 1;
        String filename = String.format(FILE_NAME+"_%03d",counter);
        boolean running = true;
        while(running) {            
            try {
                Files.createFile(Paths.get(filename));
                running = false;                
            }
            catch(FileAlreadyExistsException e) {
                running = true;                
                filename = String.format(FILE_NAME+"_%03d",counter);
                System.err.printf("Datei umbenannt in: %s%n",filename);
            }
            catch(IOException e){
                System.err.printf("Konnte Datei %s nicht erstellen: %s%n",filename,e);
                System.exit(0);
            }    
            counter++;
        }        
        
        PrintWriter out = null;
        try {
            out = new PrintWriter(filename);
            out.println(data);
        }
        catch(IOException e) {
            System.err.printf("Konnte nicht in Datei %s schreiben: %s%n",filename,e);
        }
        finally {
            out.close();     
        }
    }
}