package _20201113_String_Pool;

public class Stringpool_Test {
    public static void main(String[] args) {
        System.out.println("Hello World");

        String str1 = "Hello World";
        System.out.printf("str1 hash: %s%n",str1.hashCode());
        System.out.printf("str1 id hash: %s%n",System.identityHashCode(str1));

        String str2 = "Hello World";
        System.out.printf("str2 hash: %s%n",str2.hashCode());
        System.out.printf("str2 id hash: %s%n",System.identityHashCode(str2));

        String str3 = new String("Hello World");
        System.out.printf("str3 hash: %s%n",str3.hashCode());
        System.out.printf("str3 id hash: %s%n",System.identityHashCode(str3));    

        if(str1 == str2) System.out.println("Is the same!");
        else System.out.println("Not the same!");

        if(str1 == str3) System.out.println("Is the same!");
        else System.out.println("Not the same!");

        if(str1.equals(str3)) System.out.println("Is the same!");
        else System.out.println("Not the same!");
    }
}
