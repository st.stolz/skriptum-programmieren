package at.video;

import uk.co.caprica.vlcj.player.component.EmbeddedMediaPlayerComponent;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.JToggleButton;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.sun.jna.platform.WindowUtils;

public class App extends JFrame {
   private static final long serialVersionUID = 1L;
   private static final String TITLE = "My First Media Player";
   private static final String VIDEO_PATH = "/home/ststolz/Videos/Screenrecordings/2021-01-03 GNS3 Network.mkv";
   private final EmbeddedMediaPlayerComponent mediaPlayerComponent;
   private JButton playButton;
   private JToggleButton overlayButton;
   private boolean overlayEnabled = false;

   private final Overlay overlay;

   public App(String title) {
      super(title);
      mediaPlayerComponent = new EmbeddedMediaPlayerComponent();	

      overlay = new Overlay(this); 
      mediaPlayerComponent.mediaPlayer().overlay().set(overlay);
   }

   public void initialize() {
      this.setBounds(100, 100, 600, 400);
      this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      this.addWindowListener(new WindowAdapter() {
         @Override
         public void windowClosing(WindowEvent e) {
            mediaPlayerComponent.release();
            System.exit(0);
         }
      });    	
      JPanel contentPane = new JPanel();
      contentPane.setLayout(new BorderLayout());   	 
      contentPane.add(mediaPlayerComponent, BorderLayout.CENTER);

      JPanel controlsPane = new JPanel();
      playButton = new JButton("Play");
      controlsPane.add(playButton);    	
      contentPane.add(controlsPane, BorderLayout.SOUTH);
      playButton.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            mediaPlayerComponent.mediaPlayer().controls().play();
         }
      });   
      
      
      overlayButton = new JToggleButton("Toggle Overlay");
      controlsPane.add(overlayButton); 

      overlayButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
           toggleOverlay();
        }
     });

      this.setContentPane(contentPane);
      this.setVisible(true);
   }
   public void toggleOverlay(){
      overlayEnabled = !overlayEnabled;
      mediaPlayerComponent.mediaPlayer().overlay().enable(overlayEnabled);
   }
   public void loadVideo(String path) {
      mediaPlayerComponent.mediaPlayer().media().startPaused(path);   	
   }
   public static void main( String[] args ){
      try {
         UIManager.setLookAndFeel(
         UIManager.getSystemLookAndFeelClassName());
      } 
      catch (Exception e) {
         System.out.println(e);
      }
      App application = new App(TITLE);
      application.initialize(); 
      application.setVisible(true);  
      application.loadVideo(VIDEO_PATH);
      for (int i = 0; i < 10; i++) {
         application.toggleOverlay();
         try {
            Thread.sleep(1000);
         } catch (InterruptedException e) {
            e.printStackTrace();
         }
      }
   }
}

class Overlay extends Window {
    public Overlay(Window owner) {
       super(owner, WindowUtils.getAlphaCompatibleGraphicsConfiguration());
       setBackground(new Color(0, 0, 0, 0));
    }
    @Override
    public void paint(Graphics g) {
       super.paint(g);
       Graphics2D g2 = (Graphics2D)g;
       g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
       GradientPaint gp = new GradientPaint(
          180.0f, 280.0f,
          new Color(255, 255, 255, 255),
          250.0f,
          380.0f,
          new Color(255, 255, 0, 0)
       );
       g2.setPaint(gp);
       g2.setFont(new Font("Serif", Font.PLAIN,32));
       g2.drawString("TutorialsPoint",200, 200); 
    }
 }