import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;
import javafx.fxml.Initializable;
import java.util.ResourceBundle;
import java.net.URL;
 
public class FXMLExampleController implements Initializable {
    @FXML
    private Text actiontarget;
    
    @FXML
    private TextField userName;
    
    @FXML 
    protected void handleSubmitButtonAction(ActionEvent event) {
        actiontarget.setText("User: "+userName.getText());
        User user = new User(userName.getText());
        RegisterDbWork.registerUser(user);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("Hello World");
    }

}