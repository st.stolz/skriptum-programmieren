package _20210205_DAO_mysql_notReady.business;

import java.util.Optional;

import _20210205_DAO_mysql_notReady.persistence.Dao;
import _20210205_DAO_mysql_notReady.persistence.UserDao;
import _20210205_DAO_mysql_notReady.persistence.models.User;

public class UserApplication {

    private static Dao<User> userDao;

    public static void main(String[] args) {
        userDao = new UserDao();
        
        User user1 = getUser(0);
        System.out.println("Erste Ausgabe: "+user1);
        userDao.update(user1, new String[]{"Jake", "jake@domain.com"});
        
        User user2 = getUser(1);
        userDao.delete(user2);
        userDao.save(new User("Julie", "julie@domain.com"));
        
        userDao.getAll().forEach(user -> System.out.println(user));
    }

    private static User getUser(long id) {
        Optional<User> user = userDao.get(id);
        
        return user.orElse(
          new User("non-existing user", "no-email"));
    }
}