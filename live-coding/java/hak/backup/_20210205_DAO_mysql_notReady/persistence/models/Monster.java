package _20210205_DAO_mysql_notReady.persistence.models;

public class Monster {

    private String race;
    private int power;
    public Monster(String race, int power) {
        this.race = race;
        this.power = power;
    }
    public String getRace() {
        return race;
    }
    public void setRace(String race) {
        this.race = race;
    }
    public int getPower() {
        return power;
    }
    public void setPower(int power) {
        this.power = power;
    }

    
    
}
