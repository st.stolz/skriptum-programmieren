package _20200303_Exceptions_Files_Test_Exists;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Example with Exception handling with room for improvement
 */
public class TestFileExists {
   public static void main(String[] args){

      System.out.println("Hello World!");
      
      try{
         Files.createFile(Paths.get("my_file.md"));
      }
      catch(IOException e){
         System.out.printf("Problem creating File %n");
         e.printStackTrace();
      }
   } 
}