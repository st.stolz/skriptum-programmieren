import java.util.HashMap;
import java.util.LinkedList;

public class TestTheMap
{
    private LinkedList<String> myLinkedList;
    private HashMap<Person,String> myHashMap;
        
    public TestTheMap()
    {
        myLinkedList = new LinkedList<>();
        myHashMap = new HashMap<>();
    }
    
    public void addNewElement(Person index, String data){
        myHashMap.put(index, data);
        myLinkedList.add(data);
    }
}
