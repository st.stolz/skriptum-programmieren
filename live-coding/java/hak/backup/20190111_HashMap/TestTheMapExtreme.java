import java.util.HashMap;
import java.util.LinkedList;

public class TestTheMapExtreme
{
    private HashMap<Person,MyLinkedList> myHashMap;
        
    public TestTheMapExtreme()
    {
        myHashMap = new HashMap<>();
    }
    
    public void addNewElement(Person index, MyLinkedList data){
        myHashMap.put(index, data);
    }
}
