import java.util.LinkedList;

public class MyLinkedList
{
    private LinkedList<String> linkedList;

    public MyLinkedList()
    {
        linkedList = new LinkedList<>();
    }

    public void addElement(String data){
        linkedList.add(data);
    }
}
