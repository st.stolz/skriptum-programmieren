import java.util.ArrayList;

public class Lieferant
{
    private ArrayList<Artikel> artikelListe;
    String name;

    public Lieferant(String name)
    {
        artikelListe = new ArrayList<>();
        this.name = name;
  
    }
    
    public String getName(){
        return name;
    }
    
    public void addArtikel(Artikel artikelToAdd){
        artikelListe.add(artikelToAdd);
    }
    
    public ArrayList<Artikel> artikelSuche(String suchString){
        ArrayList<Artikel> returnArtikel = new ArrayList<>();
        for(Artikel artikelAktuell : artikelListe){
            if(artikelAktuell.getName().toLowerCase().contains(suchString.toLowerCase())){
                returnArtikel.add(artikelAktuell);
            }
        }
        return returnArtikel;
    }
    
}
