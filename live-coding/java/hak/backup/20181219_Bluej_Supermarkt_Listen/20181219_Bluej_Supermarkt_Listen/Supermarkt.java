import java.util.ArrayList;

public class Supermarkt
{
    private ArrayList<Lieferant> lieferantenListe;
    
    public Supermarkt(){
        lieferantenListe = new ArrayList<>();
    }
    
    public void addLieferant(Lieferant lieferantToAdd){
        lieferantenListe.add(lieferantToAdd);
    }
    
    public void artikelSuche(String suchbegriff){
        for(Lieferant lieferantAktuell : lieferantenListe){
            ArrayList<Artikel> artikelListe = lieferantAktuell.artikelSuche(suchbegriff);
            System.out.print(lieferantAktuell.getName());
            if(artikelListe != null){
                System.out.println(" - " + artikelListe.size() + "Artikel gefunden");
                for(Artikel artikelAktuell : artikelListe){
                    System.out.println("  * "+artikelAktuell.getName());
                }
            }
        }
    }
        
}
