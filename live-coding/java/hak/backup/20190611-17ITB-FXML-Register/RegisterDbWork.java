//STEP 1. Import required packages
import java.sql.*;

public class RegisterDbWork {
   
   static final String DB_URL = "jdbc:mysql://localhost/serp";

   //  Database credentials
   static final String USER = "root";
   static final String PASS = "123";
   
   public static void registerUser(User user) {
       
   Connection conn = null;
   Statement stmt = null;
   
   try{
      //STEP 3: Open a connection
      System.out.println("Connecting to a selected database...");
      conn = DriverManager.getConnection(DB_URL, USER, PASS);
      System.out.println("Connected database successfully...");
      
      //STEP 4: Execute a query
      System.out.println("Inserting records into the table...");
      stmt = conn.createStatement();
      
      String sql = String.format("INSERT INTO customers (name) VALUES ('%s')",user.getUsername());
      stmt.executeUpdate(sql);
      
      System.out.println("Inserted records into the table...");

   }catch(SQLException se){
      //Handle errors for JDBC
      se.printStackTrace();
   }catch(Exception e){
      //Handle errors for Class.forName
      e.printStackTrace();
   }finally{
      //finally block used to close resources
      try{
         if(stmt!=null)
            conn.close();
      }catch(SQLException se){
      }// do nothing
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }//end finally try
   }//end try
   System.out.println("Goodbye!");
}//end main
}//end JDBCExample