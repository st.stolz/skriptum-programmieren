package _20210226_enum;

public class _01_enum {
    public static void main(String[] args) {
        String farbe = "Gelb";

        if(farbe == "Geld"){
            System.out.println("Die Farbe ist gelb!");
        }
        else if (farbe == "Rot"){
            System.out.println("Die Farbe ist rot!");
        }
        else {
            System.out.println("Fehler! Diese Farbe existiert nicht!");
        }

        Farbe farbeEnum = Farbe.ROT;

        if(farbeEnum == Farbe.GELB){
            System.out.println("Die Farbe ist gelb!");
        }
        else if (farbeEnum == Farbe.ROT){
            System.out.println("Die Farbe ist rot!");
        }

        Shapes shape = Shapes.CIRCL;

        switch(shape){
            case CIRCL: System.out.println(Shapes.CIRCL+"!"); break;
            case RECTANGLE: System.out.println("Rectangle!"); break;
            case SQUARE: System.out.println("Square!"); break;
            default: System.out.println("Fehler! Diese Farbe existiert nicht!"); break;
        }

        for (Shapes shape2 : Shapes.values()) {
            System.out.println(shape2);
        }

    }
}

enum Farbe {
    GELB, ROT
}