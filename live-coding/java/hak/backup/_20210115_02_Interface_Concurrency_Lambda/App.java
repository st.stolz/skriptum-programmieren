package _20210115_02_Interface_Concurrency_Lambda;

public class App {

    public static void main(String[] args) {
        System.out.printf("%s Started %n", Thread.currentThread().getName());

        Object syncObject = new Object();
        WorkerClass wc = new WorkerClass(syncObject);

        Runnable runner = () -> {
            wc.work();
        };

        Thread t1 = new Thread(runner);
        t1.start();

        Thread t2 = new Thread(runner);
        t2.start();

        Thread t3 = new Thread(runner);
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.printf("Counted %d times %n",DataStorage.counter);
    }

}

class WorkerClass {

    Object syncObject;

    public WorkerClass( Object syncObject){
        this.syncObject = syncObject;
    }

    public void work(){
        for (int i = 0; i < 100000; i++) {
            synchronized(syncObject){
                DataStorage.counter++;
            }            
            //System.out.printf("%s Added +1 %n",Thread.currentThread().getName());
        }        
    }

}

class DataStorage {
    public static int counter = 0;
}

