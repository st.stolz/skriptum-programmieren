

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class ProduktTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ProduktTest
{
    /**
     * Default constructor for test class ProduktTest
     */
    public ProduktTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    public void myTest()
    {
        Produkt produkt1 = new Produkt(null, 5);
        produkt1.showInfo();
    }

    @Test
    public void myTest2()
    {
        Produkt produkt1 = new Produkt("Stefan", 5);
        assertEquals(0, produkt1.getNumberOfComments());
        Comment comment1 = new Comment("Stefan", "123", 3);
        assertEquals(true, produkt1.addComment("Stefan", "2134", 4));
        assertEquals(1, produkt1.getNumberOfComments());
    }

    @Test
    public void myTest3()
    {
        Produkt produkt1 = new Produkt("Stefan", 3);
        produkt1.findMostHelpfulComment();
    }
}



