package _20200629_Scanner_Wiederholung;

/**
 * Beschreiben Sie hier die Klasse MessagePost.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class MessagePost extends Post
{
    

    /**
     * Konstruktor für Objekte der Klasse MessagePost
     */
    public MessagePost()
    {
        // Instanzvariable initialisieren
        super.x = 0;
    }

    /**
     * Ein Beispiel einer Methode - ersetzen Sie diesen Kommentar mit Ihrem eigenen
     * 
     * @param  y    ein Beispielparameter für eine Methode
     * @return        die Summe aus x und y
     */
    public int beispielMethode(int y)
    {
        // tragen Sie hier den Code ein
        return x + y;
    }
}
