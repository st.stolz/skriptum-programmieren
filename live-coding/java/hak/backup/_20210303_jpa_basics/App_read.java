package _20210303_jpa_basics;

import java.util.List;

import _20210303_jpa_basics.entities.AnotherThing;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;

public class App_read {
    public static void main(String[] args) {
        // Setup the entity manager
        EntityManagerFactory factory =   Persistence.createEntityManagerFactory("example");
        EntityManager em = factory.createEntityManager();
        
        // Fetch them
        TypedQuery<AnotherThing> q = em.createQuery("select ting from AnotherThing ting", AnotherThing.class);
        //em.find(arg0, arg1)
        List<AnotherThing> results = q.getResultList();
        for (AnotherThing thing : results) {
            System.out.println(thing.getId() + ": " + thing.getText());
        }
        
        em.close();
        factory.close();
    }
}
