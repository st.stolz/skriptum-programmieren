package _20200422_Exceptions_Files_Size;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Example with Exception handling with room for improvement
 */
public class TestFileExists {
   public static void main(String[] args){

      Path filePath = Paths.get("my_file.md");
      
      BufferedWriter writer = null;

      try {
         writer = Files.newBufferedWriter(filePath, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);         
      } catch (IOException e) {
         e.printStackTrace();
      }
      
      try {
         writer.write("Hello World!",0,11);
         writer.flush();
      } catch (IOException e) {
         e.printStackTrace();
      }

      try {
         System.out.println(Files.size(filePath));
      } catch (IOException e) {
         e.printStackTrace();
      }
      
   } 
}