
public class Werkzeug
{
    private int preis;
    private String name;
    private int lagerstand;
    
    public Werkzeug(int preis, int lagerstand, String name)
    {
        this.preis = preis;
        this.lagerstand = lagerstand;
        this.name = name;
    }

    public int getPreis(){
        return preis;
    }
    
    public String getName(){
        return name;
    }
    
    public int getLagerstand(){
        return lagerstand;
    }
}
