import java.util.ArrayList;
import java.util.Iterator;

public class Lager
{
    private ArrayList<Werkzeug> werkzeugListe;
    
    public Lager()
    {
        werkzeugListe = new ArrayList<>();
    }

    public int berechneLagerstand(){
        int lagerstand = 0;
        for(Werkzeug werkzeug : werkzeugListe){
            lagerstand = lagerstand + werkzeug.getLagerstand();
        }
        return lagerstand;
    }    
    
    
    public void werkzeugEntnahme(Werkzeug werkzeugZurEntnahme){
        Iterator<Werkzeug> iterator = werkzeugListe.iterator();
        while(iterator.hasNext()){   
            Werkzeug werkzeug = iterator.next();
        }
    }
}
