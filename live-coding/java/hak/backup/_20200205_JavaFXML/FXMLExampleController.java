import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import javafx.scene.control.*;
 
public class FXMLExampleController {
    @FXML private Text actiontarget;
    @FXML private TextField userName;
    
    @FXML protected void handleSubmitButtonAction(ActionEvent event) {
        actiontarget.setText("Sign in button pressed");
        userName.setText("Test");
    }

}