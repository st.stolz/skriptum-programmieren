package _20201106_Vererbung_Polymorphie;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        Writing_Instrument pen = new Writing_Instrument();
        System.out.printf("%s %n","## Normal Writing Instrument");
        pen.write("Zeile 1");
        pen.write("Zeile 2");
        pen.erase();
        pen.write("Zeile 3");
        pen.printInfo();
        System.out.printf("%s %n","## Now a Pencil!");
        pen = new Pencil();
        pen.write("Zeile 1");
        pen.write("Zeile 2");
        pen.erase();
        pen.write("Zeile 3");
        pen.printInfo();
    }    
}

class Writing_Instrument {
    private ArrayList<String> writtenData = new ArrayList<String>();

    public void write(String data){
        System.out.printf("Writing: %s %n",data);
        writtenData.add(data);
    }

    public ArrayList<String> getWrittenData(){
        return writtenData;
    }

    public void setWrittenData(ArrayList<String> writtenData){
        this.writtenData = writtenData;
    }
    
    public void printInfo(){
        System.out.printf("%s %n", "Stored Data:");
        for(String line : writtenData){
            System.out.printf("%s %n", line);
        }
    }

    public void erase() {
        System.out.printf("%s %n","Erase not possible");
    }
}

class Pencil extends Writing_Instrument {
    ArrayList<String> erasedData = new ArrayList<String>();

    @Override
    public void erase() {
        System.out.printf("%s %n","Erasing last line");
        removeLastData();        
    }
    
    public void removeLastData(){
        ArrayList<String> writtenData = super.getWrittenData();
        erasedData.add(writtenData.get(writtenData.size()-1));
        writtenData.remove(writtenData.size()-1);
        super.setWrittenData(writtenData);
    }

    @Override
    public void printInfo(){
        super.printInfo();
        System.out.printf("%s %n", "Erased Data:");
        for(String line : erasedData){
            System.out.printf("%s %n", line);
        }
    }
}
