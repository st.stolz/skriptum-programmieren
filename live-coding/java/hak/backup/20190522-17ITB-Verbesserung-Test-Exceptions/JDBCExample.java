import java.sql.*;

public class JDBCExample {
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   static final String DB_URL = "jdbc:mysql://localhost/";

   static final String USER = "root";
   static final String PASS = "123";
   
   public static void main(String[] args) {
   Connection conn = null;
   Statement stmt = null;
   int counter = 0;
   boolean running = true;
   while(counter < 10 && running){
       counter++;
       try{           
          Class.forName("com.mysql.jdbc.Driver");
          
          conn = DriverManager.getConnection(DB_URL, USER, PASS);
          
          stmt = conn.createStatement();
          
          String sql = "CREATE DATABASE STUDENTS";
          stmt.executeUpdate(sql);
          System.out.println("Database created successfully...");
          running = false;
       }
       catch(SQLTimeoutException se){
          //Handle errors for JDBC
          se.printStackTrace();
          running = false;
       }
       catch(SQLException se){
          //Handle errors for JDBC
          se.printStackTrace();
          running = true;
       }catch(ClassNotFoundException e){
          //Handle errors for Class.forName
          e.printStackTrace();
          running = false;
        }
       finally{
          try{
             if(stmt!=null)
                stmt.close();
          }catch(SQLException se2){
          }
          try{
             if(conn!=null)
                conn.close();
          }catch(SQLException se){
             se.printStackTrace();
          }
       }
       System.out.println("Goodbye!");
    }
}
}
