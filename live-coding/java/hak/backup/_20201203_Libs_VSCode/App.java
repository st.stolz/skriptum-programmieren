package _20201203_Libs_VSCode;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class App {
    public static void main(String[] args) throws Exception {

        String json = "{\"brand\":\"Jeep\", \"doors\": 3}";
        json = "{\"brand\":\"Honda\", \"door\": 4, \"ps\": 10}";

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        Car car = gson.fromJson(json, Car.class);

        System.out.printf("Brand: %s - Doors: %s - PS: %s %n",car.brand,car.seppele, car.ps);
    
        Car car2 = new Car();
        car2.brand = "Toyota";
        car2.ps = 200;
        car2.seppele = 6;

        System.out.printf("JSON: %s %n",gson.toJson(car2));
    
    }
}

class Car {
    @Expose(serialize = true, deserialize = true) 
    public String brand = null;

    @SerializedName("door") 
    @Expose(serialize = true, deserialize = true) 
    public int    seppele = 0;

    @Expose(serialize = true, deserialize = false) 
    public Integer ps = null;

    public String calculateHash(String strToHash){
        String returnHash = "";
        try {
            byte[] bytesOfMessage = strToHash.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                String hex = Integer.toHexString(0xff & thedigest[i]);
    			if(hex.length() == 1) 
    			     hexString.append('0');
    			hexString.append(hex);
            }
            returnHash = hexString.toString();
        }
        catch(UnsupportedEncodingException | NoSuchAlgorithmException e){
            e.printStackTrace();
            System.exit(1);
        }
        return returnHash;        
    }

}