package _20200506_Exceptions_Files_Test_Exists_Solution;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Example with Exception handling with room for improvement
 */
public class TestFileExists_solution {

   public static void main(String[] args){

      Path filePath = Paths.get("my_file.md");
      
      BufferedWriter writer = null;

      try {
         writer = Files.newBufferedWriter(filePath, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);         
      } catch (IOException e) {
         System.err.println("Fehler: Die Datei konnte nicht zum schreiben geöffnet werden:");
         e.printStackTrace();
      }
      
      try {
         writer.write("Hello World!",0,11);
         writer.flush();
      } catch (IOException e) {
         System.err.println("Fehler: Es konnte nicht in die Datei geschrieben werden!");
         e.printStackTrace();
      }

      long size = 0;

      try {
         size = Files.size(filePath);
         System.out.println(size);
      } catch (IOException e) {
         System.err.println("Konnte die Dateigröße nicht feststellen. Es gab ein Problem mit dem Zugriff auf die Datei!");
         e.printStackTrace();
      }

      if(size > 25){
         System.out.println("Dateigröße größer 25");

         SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
         Date now = new Date();
         String strDate = sdf.format(now);

         boolean failed = true;

         int counter = 0;

         while(failed && counter < 100){

            Path filePathTarget = Paths.get(strDate+"-"+counter+"-my_file.md");

            try {
               Files.move(filePath, filePathTarget);
               System.out.println("Datei erfolgreich umbenannt:");
               System.out.println(filePathTarget.toString());
               failed = false;        
               
            } 

            catch (FileAlreadyExistsException e) {
               //System.err.println("Konnte Datei nicht verschieben, weil Ziel bereits existiert:");
               //System.err.println(filePathTarget.toString());
               //System.err.println("Versuche Datei umzubennen.");
               failed = true;
            }
            
            catch (IOException e) {
               e.printStackTrace();
               failed = false;
            }

            counter ++;       

         }

          

      }
      
   } 
}