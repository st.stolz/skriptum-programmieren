package _20210318_DAO_Wiederholung.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import _20210318_DAO_Wiederholung.persistence.models.User;

public class UserDao implements Dao<User> {
    
    public UserDao() {
        
        System.out.println("Connecting ...");
        DbController.connect();
        System.out.println("Creating Table ...");
        DbController.createUserTable();

        
    }
    
    @Override
    public Optional<User> get(long id) {
        // TODO: return Optional.ofNullable(users.get((int) id));
        return Optional.of(new User("Sepp","sepp@gmail.com"));
    }
    
    @Override
    public List<User> getAll() {
        // TODO: return users;
        return new ArrayList<User>();
    }
    
    @Override
    public void save(User user) {
        DbController.insertUser(user.getName(), user.getEmail());
    }
    
    @Override
    public void update(User user, String[] params) {

        user.setName(Objects.requireNonNull(
          params[0], "Name cannot be null"));
        user.setEmail(Objects.requireNonNull(
          params[1], "Email cannot be null"));
        
        // von Tutorial korrigiert, dass User nicht doppelt in Liste
        //users.add(user);
    }
    
    @Override
    public void delete(User user) {
        // TODO: users.remove(user);
    }
}

class DbController {
    
    static Connection conn;

    public static void insertUser(String name, String email){
        String sql = "INSERT INTO users(name,email) VALUES(?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void createUserTable() {
        String sql = "CREATE TABLE IF NOT EXISTS users (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	email text\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table warehouse is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void connect() {
        String url = "jdbc:sqlite:./src/_20210205_DAO_mysql_notReady/mydb.db";
        try {
            conn = DriverManager.getConnection(url);            
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }         
    }

}
