package _20210128_SQLite;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class App {    
    public static void main(String[] args) {
        DbController.connect();

        DbController.createNewTable();

        DbController.insert("HAK Imst", 1.234);
        DbController.insert("HTL Imst", 4.321);

        DbController.close();
    }
}

class DbController {

    static Connection conn;

    public static void createNewTable() {
        String sql = "CREATE TABLE IF NOT EXISTS warehouses (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	capacity real\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table warehouse is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insert(String name, double capacity){
        String sql = "INSERT INTO warehouses(name,capacity) VALUES(?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.setDouble(2, capacity);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void connect() {

        String url = "jdbc:sqlite:./src/_20210128_SQLite/mydb.db";
        try {
            conn = DriverManager.getConnection(url);
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }         
    }

    public static void close(){
        
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
}