
public class State
{
    private int state;
    private String created;
    private String entity_id;
    
    public State(int state, String created, String entity_id)
    {
        this.state = state;
        this.created = created;
        this.entity_id = entity_id;
    }

    public int getState(){
        return state;
    }
    public String getCreated(){
        return created;
    }
    public String getEntity_id(){
        return entity_id;
    }
}
