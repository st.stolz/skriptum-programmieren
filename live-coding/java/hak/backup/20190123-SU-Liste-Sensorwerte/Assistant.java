import java.util.ArrayList;

public class Assistant
{
    private ArrayList<State> stateList;

    
    public Assistant()
    {
        stateList = new ArrayList<>();
    }
    
    public void addState(int state, String entity_id){
        State stateToAdd = new State(state, "10:11", entity_id);
        stateList.add(stateToAdd);
    }

    public void printSensorValues(String entity_id){
        for(State state : stateList){
            if(entity_id.equals(state.getEntity_id())){
                System.out.print(state.getCreated());
                System.out.print(": ");
                System.out.println(state.getState());
            }
        }
    }
}
