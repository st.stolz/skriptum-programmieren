
public class Buch extends Medium
{
    private int seitenZahl;
    
    public Buch(int seitenZahl, String erstellerName, int erscheinungsJahr, String verlag, String titel)
    {        
        super(erstellerName,erscheinungsJahr,verlag,titel);
        this.seitenZahl = seitenZahl;
    }

    @Override
    public String anzeigen(){
        String returnString = "";
        returnString += super.anzeigen();
        returnString += String.format("Seitenzahl: %s \n", seitenZahl);
        return returnString;
    }
}
