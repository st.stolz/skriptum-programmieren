
public class Cd extends Medium
{
    private int cdDauer;
    private int anzahlLieder;
    
    public Cd(int cdDauer, int anzahlLieder, String erstellerName, int erscheinungsJahr, String verlag, String titel)
    {        
        super(erstellerName,erscheinungsJahr,verlag,titel);
        this.cdDauer = cdDauer;
        this.anzahlLieder = anzahlLieder;
    }

    
}
