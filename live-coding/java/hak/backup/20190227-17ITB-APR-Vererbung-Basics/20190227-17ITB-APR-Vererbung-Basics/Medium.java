public class Medium
{
    private String erstellerName;
    private int erscheinungsJahr;
    private String verlag;
    private String titel;
    
    public Medium(String erstellerName, int erscheinungsJahr, String verlag, String titel){
        this.erstellerName = erstellerName;
        this.erscheinungsJahr = erscheinungsJahr;
        this.verlag = verlag;
        this.titel = titel;
    }
    
    public String anzeigen(){
        String returnString = "";
        returnString = returnString + "Ersteller: " + erstellerName + "\n";
        returnString += String.format("Erscheinungsjahr: %s \n", erscheinungsJahr);
        returnString += String.format("Verlag: %s \n", verlag);
        returnString += String.format("Titel: %s \n", titel);
        return returnString;
    }
}