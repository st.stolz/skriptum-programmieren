import java.util.LinkedList;

public class Bibliothek
{
    private LinkedList<Medium> mediumListe;
    
    public Bibliothek()
    {
        mediumListe = new LinkedList<>();
    }

    public void addMedium(Medium medium){
        mediumListe.add(medium);
    }
    
    public void bibliothekAusgeben(){
        for(Medium medium : mediumListe){
            System.out.println(medium.anzeigen());
        }
    }
}
