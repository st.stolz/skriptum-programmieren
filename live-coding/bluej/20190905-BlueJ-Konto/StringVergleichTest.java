

public class StringVergleichTest
{
    
    String vorName;

    
    public StringVergleichTest()
    {
        vorName = "Stefan";
    }

    
    public boolean vergleich1(String vorName)
    {
        if(this.vorName == vorName) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public boolean vergleich2()
    {
        String vorName = "Stefan";
        System.out.println(vorName);
        if(this.vorName == vorName) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public boolean vergleich3()
    {
        String vorName = new String("Stefan");
        System.out.println(vorName);
        if(this.vorName == vorName) {
            return true;
        }
        else {
            return false;
        }
    }
}
