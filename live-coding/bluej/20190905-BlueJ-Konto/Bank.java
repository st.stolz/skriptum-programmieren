import java.util.LinkedList;

public class Bank
{
    private LinkedList<Konto> konten;

    public Bank()
    {
        konten = new LinkedList<>();
    }

    public void printKonten()
    {
        System.out.println("Anzahl Konten: "+konten.size());
    }
    
    public void addKonto(Konto konto) {
        konten.add(konto);
    }
}
