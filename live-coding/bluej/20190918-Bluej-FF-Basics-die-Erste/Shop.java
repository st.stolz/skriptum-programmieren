

public class Shop
{
    // instance variables - replace the example below with your own    
    private String shopName;
    private Position position;

    /**
     * Constructor for objects of class Shop
     */
    public Shop(String shopName, Position position)
    {
        this.shopName = shopName;
        this.position = position;
    }

    public String getShopName(){
        return shopName;
    }
    
    public void setShopName(String shopName){
        this.shopName = shopName;
    }
    
    public int getX(){
        int x;
        x = this.position.getX();
        return x;
    }
    
    public int getY(){
        return this.position.getY();
    }
    
    public Position getPostion(){
        return position;
    }
}
