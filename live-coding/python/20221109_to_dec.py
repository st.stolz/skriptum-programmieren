import math

def main():
    input_str = input("Number to convert: ")
    reversed_input_str = input_str[::-1]
    base = input("Base of Number: ")

    number = 0
    length = len(reversed_input_str)
    for i in range(length):
        # Den ersten Char verwarbeiten
        # (lower(), dass nicht zwischen groß und klein unterschieden werden muss)
        char_i = reversed_input_str[i].lower()
        # Testen, ob der Char in einen Integer gecastet werden kann
        number_from_char = 0
        try:
            number_from_char = int(char_i)
        # Ansonsten den Buchstaben mit Hilfe des ASCII Wertes umrechnen
        # auf a = 10, b = 11, ...
        except ValueError:
            number_from_char = ord(char_i)-97+10
        number_i = number_from_char * math.pow(int(base), i)
        number += number_i
        # print(int(number_i))

    print(int(number))

if __name__ == "__main__":
    main()
