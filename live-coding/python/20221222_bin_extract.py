import math

def main():
    dec_input_str = input("Number to extract Bits: ")   
    bin_extract(int(dec_input_str))

def bin_extract(dec_input):
    dec_process = dec_input
    mask = 1
    for i in range(8):
        bin_i = dec_process & mask
        dec_process = dec_process >> 1
        print(f'{i}: {bin_i}')

if __name__ == "__main__":
    main()
