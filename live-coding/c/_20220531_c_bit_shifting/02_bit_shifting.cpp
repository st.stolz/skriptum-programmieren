#include <stdio.h>

int main(void){

    int zahl = 10;

    zahl = zahl << 1;
    printf("%d \n", zahl);

    zahl = zahl >> 1;
    printf("%d \n", zahl);

    zahl <<= 1;
    printf("%d \n", zahl);

    zahl = 13;
    int mask = 1;

    printf("%d \n", zahl&mask);

    zahl >>= 1;

    printf("%d \n", zahl&mask);

    return 0;

}