#include <iostream>
#include <stdio.h>
#include <stdlib.h>

int main(void){

    int zahl = 10;

    printf("%d \n", zahl);
    printf("------------------ \n");

    // Einen Pointer erstellen, der auf die Speicherstelle der Zahl zeigt
    int* zahl_p = & zahl;

    printf("zahl_p: %d \n", zahl_p);
    printf("*zahl_p: %d \n", *zahl_p);
    printf("zahl: %d \n", zahl);
    printf("------------------ \n");

    // Den Wert ändern, auf den der Pointer zeigt
    *zahl_p = 12;

    printf("zahl_p: %d \n", zahl_p);
    printf("*zahl_p: %d \n", *zahl_p);
    printf("zahl: %d \n", zahl);
    printf("------------------ \n");

    // Den Pointer auf eine neue int Speicherstelle zeigen lassen
    // Es wird gleich Speicher für zwei int Zahlen reserviert (Array der Länge 2)
    zahl_p = (int*) malloc(sizeof(int) * 2);
    *zahl_p = 13;
    *(zahl_p + 1) = 41;

    printf("zahl_p: %d \n", zahl_p);
    printf("*zahl_p: %d \n", *zahl_p);
    printf("*(zahl_p + 1): %d \n", *(zahl_p + 1));
    // alternativer Zugriff mit Array Schreibweise
    printf("zahl_p[1]: %d \n", zahl_p[1]);
    printf("zahl: %d \n", zahl);
    printf("------------------ \n");


    return 0;

}