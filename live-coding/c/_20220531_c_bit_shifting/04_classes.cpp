#include <stdio.h>

class Human {

    public: 
        Human();
        void eat(int cal);
        int getEnergy();

    private:
        int energy;

};

Human::Human() { energy = 0; }

void Human::eat(int energy){
    this->energy = energy;
}

int Human::getEnergy(){ return this->energy; }

int main(void){

    Human human1;

    printf("Energy: %d \n", human1.getEnergy());

    human1.eat(5);

    printf("Energy: %d \n", human1.getEnergy());

    return 0;
}