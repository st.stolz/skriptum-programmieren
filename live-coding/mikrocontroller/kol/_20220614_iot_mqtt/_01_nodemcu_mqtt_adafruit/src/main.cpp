// http://docs.platformio.org/en/latest/ide/atom.html#quick-start

#include "Arduino.h"

// nodemcu Library beim ESP-8266:
#include <ESP8266WiFi.h>
// beim ESP-32:
//#include <WIFI.h>

// MQTT Library
#include <PubSubClient.h>

#include <Credentials.h>

byte greenLedPin = D1;
byte errLedPin = D2;

char ssid[] = WIFI_SSID;
char pass[] = WIFI_PW;

WiFiClient wifiClient;

// mqtt configuration
const char* server = "io.adafruit.com";
const char* topic = "STOL/feeds/edu.2017-pos1ext";
const char* clientName = "com.ststolz.nodemcu._20180104_MQTT_Connection_Read";

PubSubClient client(wifiClient);

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  String data;
  for (int i = 0; i < length; i++) {
      data += (char)payload[i];
  }
  if(strcmp(data.c_str(), "HIGH") == 0){
    digitalWrite(greenLedPin, HIGH);
  }
  else if(strcmp(data.c_str(), "LOW") == 0){
    digitalWrite(greenLedPin, LOW);
  }
}


void mqttReConnect() {
    while (!client.connected()) {
        Serial.print("Attempting MQTT connection...");
        // Attempt to connect
        if (client.connect(clientName, MQTT_USER, MQTT_KEY)) {
            Serial.println("connected");
            client.subscribe(topic);
        } else {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            delay(5000);
        }
    }
}

void setup(){

  pinMode(greenLedPin, OUTPUT);
  pinMode(errLedPin, OUTPUT);

  /* WIFI Connection */
  Serial.begin(9600);
  pinMode(errLedPin,OUTPUT);
	//Blynk.begin(auth, ssid, pass);
	WiFi.begin(ssid, pass);
	Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println(WiFi.gatewayIP());  

  client.setServer(server, 1883);
  client.setCallback(callback);
  mqttReConnect();
  delay(10);
}

void loop(){

  /* WIFI Connection */
  while (WiFi.status() != WL_CONNECTED)
	{
		digitalWrite(errLedPin,HIGH);
		delay(500);
		Serial.print(".");
	}
	digitalWrite(errLedPin,LOW);

  if (!client.connected()) {
      mqttReConnect();
  }
  client.loop();

}