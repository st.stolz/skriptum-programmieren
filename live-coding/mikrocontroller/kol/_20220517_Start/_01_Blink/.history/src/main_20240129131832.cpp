#include <Arduino.h>

int led_pin1 = 2;
int led_pin2 = 3;

void setup(){
    pinMode(led_pin1, OUTPUT);
}

void loop(){
    digitalWrite(led_pin1,HIGH);
    delay(1000);
    digitalWrite(led_pin1,LOW);
    delay(1000);
}